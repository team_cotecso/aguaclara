

class Sms
  CUENTA_TOKEN = "400488a3a8e0f5aed77d0bcc06614e74"
  SUBCUENTA_TOKEN = "71d702d7d0a03de8503e660a46275a78"


  def self.enviar_sms(solicitud, msj)
    log = Logger.new(Rails.root.to_s + '/log/sms.log')
    sms = Textveloper::Sdk.new(CUENTA_TOKEN,SUBCUENTA_TOKEN)
    if Sms::connection?
      balance = Sms::balance

      if balance > 0
        result = sms.send_sms(solicitud.solicitante.telefono_movil.gsub("-",""),msj)
        if result.count > 0
          # puts result.first.to_json
          if result.first[1]["transaccion"] == "exitosa"
            log.info "Enviado Nro: #{solicitud.solicitante.telefono_movil.gsub("+58","0")}, a las #{DateTime.now.to_s}"
          else
            log.info "Error del API. Nro: #{solicitud.solicitante.telefono_movil.gsub("+58","0")}, a las #{DateTime.now.to_s}, #{result.first[1]['mensaje_transaccion']}"
          end
        else
          log.info "Error del API. Nro: #{solicitud.solicitante.telefono_movil.gsub("+58","0")}, a las #{DateTime.now.to_s}, #{result.first[1]['mensaje_transaccion']}"
        end
      else
        log.error "Sin Balance, a las #{DateTime.now.to_s}"
      end
    end

  end


  def self.run_sms_server()
    sms = Textveloper::Sdk.new(CUENTA_TOKEN,SUBCUENTA_TOKEN)

    log = Logger.new('sms.log')

    if Sms::connection?

      balance = Sms::balance

      if balance > 0
        for_send = Sms::Outbox.where(:processed => false).order(:insertdate).limit(5)

        for_send.each do |msj|
          if balance > 0
            result = sms.send_sms(msj.number.gsub("+58","0"),msj.text)
            msj.processed_date = Time.now
            msj.processed = true
            if result.count > 0
              # puts result.first.to_json
              if result.first[1]["transaccion"] == "exitosa"
                msj.error = 0
                log.debug "Enviado Nro: #{msj.number.gsub("+58","0")}, a las #{msj.processed_date.to_s}"
              else
                msj.error = 8
                log.debug "Error del API. Nro: #{msj.number.gsub("+58","0")}, a las #{msj.processed_date.to_s}, #{result.first[1]['mensaje_transaccion']}"
              end
            else
              msj.error = 6
              log.debug "Error del API. Nro: #{msj.number.gsub("+58","0")}, a las #{msj.processed_date.to_s}, #{result.first[1]['mensaje_transaccion']}"
            end
            msj.save

            balance-=1
          else
            log.debug "Sin Balance, a las #{msj.processed_date.to_s}"
            false
          end
        end
        true
      else
        log.debug "Sin Balance, a las #{msj.processed_date.to_s}"
        false
      end
      true
    else
      log.debug "Sin Conexión"
      false
    end
  end

  def self.balance
    begin
      account = Textveloper::Sdk.new(CUENTA_TOKEN,SUBCUENTA_TOKEN)
      balance = account.account_balance
      if balance.count > 0
        return balance["puntos_disponibles"].to_i
      else
        return 0
      end
    rescue
      return 0
    end
  end

  def self.connection?
    begin
      account = Textveloper::Sdk.new(CUENTA_TOKEN,SUBCUENTA_TOKEN)
      balance = account.account_balance
      if balance.count > 0
        return true
      else
        return false
      end
    rescue
      return false
    end
  end

end
