AguaClara::Application.routes.draw do
  
  devise_for :administradores, :skip => [:registrations]
    as :administrador do
      get 'administradores/edit' => 'devise/registrations#edit', :as => 'edit_administrador_registration'
      put 'administradores/:id' => 'devise/registrations#update', :as => 'administrador_registration'            
    end
    
  mount RailsAdmin::Engine => '/admin', :as => 'rails_admin'
  
  get 'usuarios/sign_out' => 'devise/sessions#destroy', as: :logout

  get "entidades_territoriales/:id/ciudades" => 'entidades_territoriales#ciudades'
  get "entidades_territoriales/:id/municipios" => 'entidades_territoriales#municipios'
  get "entidades_territoriales/:id/parroquias" => 'entidades_territoriales#parroquias'
  get "entidades_territoriales/:id/direcciones" => 'entidades_territoriales#direcciones'
  
  get "instituciones/:id/entes_ejecutores" => 'dependents_selects#entes_ejecutores'
  get "unidades/:id/instituciones" => 'dependents_selects#instituciones'
  get "fuentes_financiamiento/:id/fondos_financiamiento" => 'dependents_selects#fondos_financiamiento'
  get "tipos_obras/:id/sub_tipos_obras" => 'dependents_selects#sub_tipos_obras'

  get "estados/:id/municipios" => 'dependents_selects#municipios'
  get "municipios/:id/parroquias" => 'dependents_selects#parroquias'
  get "parroquias/:id/sectores" => 'dependents_selects#sectores'

  get "/contratos" => "contratos#list", as: :contratos
  get "/contratos/search" => "contratos#search", as: :search_contratos

  get "/desembolsos" => "desembolsos#list", as: :desembolsos
  get "/desembolsos/search" => "desembolsos#search", as: :search_desembolsos

  resources :proyectos do
    get 'search', on: :collection
    get 'pdf', on: :member
    resources :actividades do
      get 'search', on: :collection
    end
    resources :bitacoras, controller: 'bitacora'
    resources :contratos do      
    end
    resources :desembolsos do
      get 'search', on: :collection
    end    
  end

  get "/valuaciones" => "valuaciones#list", as: :list_valuaciones
  get "/valuaciones/search" => "valuaciones#search", as: :search_valuaciones
  scope path:"/contratos/:contrato_id" do
   resources :valuaciones 
  end

  namespace "reportes" do
    resources :cantidades_obras do
        collection do
         get 'report'
       end
    end
    resources :proyectos do
        collection do
         get 'report'
       end
    end
    resources :actividades do
        collection do
         get 'report'
       end
    end
    resources :desembolsos do
        collection do
         get 'report'
       end
    end
  end

  
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
