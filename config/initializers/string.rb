class String
    def is_i?
      !!(self =~ /^[-+]?[0-9]+$/)
    end

    def to_underscore
      self.gsub(/::/, '_').
      gsub(/([A-Z]+)([A-Z][a-z])/,'\1_\2').
      gsub(/([a-z\d])([A-Z])/,'\1_\2').
      tr("-", "_").
      downcase
    end
end