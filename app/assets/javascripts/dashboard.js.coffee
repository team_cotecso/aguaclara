window.update_dashboard = ->
  $('#dashboard_ejecucion_financiera').highcharts(
    chart: 
      type: 'column'
    title:
      text: ''
    xAxis:
      categories: ['Solicitado','Aprobado','En proceso','Ejecutado']
    yAxis:
       title:
        text: '(MM)'
    plotOptions: 
      column: 
        pointPadding: 0.2
        borderWidth: 0
    series: data_ejecucion_financiera
  )

  $('#dashboard_proyectos').highcharts(
    chart: 
      plotBackgroundColor: null
    title:
      text: ''
    plotOptions: 
      pie: 
        allowPointSelect: true        
        cursor: 'pointer'
        dataLabels:
          enabled: true
          distance: 5
          format: '{point.percentage:.1f} %'
        showInLegend: true
    series: [{
       type: 'pie'
       name: 'Browser share'
       data: data_proyectos                  
    }]
  )

  $('#dashboard_fondos').highcharts(
    chart: 
      plotBackgroundColor: null
    title:
      text: ''
    xAxis:
      categories: []
    plotOptions: 
      pie: 
        allowPointSelect: true
        cursor: 'pointer'
        dataLabels:
          distance: 5        
          enabled: true
          format: '{point.percentage:.1f} %'
        showInLegend: true
    series: [{
       type: 'pie'
       name: 'Browser share'
       data: data_fondos            
    }]
  )

  $('#dashboard_desembolsos').highcharts(
    chart: 
      plotBackgroundColor: null
    title:
      text: ''
    xAxis:
      categories: []
    plotOptions: 
      pie: 
        allowPointSelect: true
        cursor: 'pointer'
        dataLabels:
          enabled: true
          distance: 5   
          format: '{point.percentage:.1f} %'
        showInLegend: true
    series: [{
       type: 'pie'
       name: 'Browser share'
       data: data_desembolsos           
    }]
  )  

  $('tspan:contains("Highcharts.com")').css('display','none')  
