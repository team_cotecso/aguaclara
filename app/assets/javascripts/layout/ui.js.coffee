$(document).on 'rails_admin.dom_ready', (e, content) ->
  
  # DatePicker
  $('.datepicker').datetimepicker
    pickTime: false
    language: 'es'

  # DependetSelects
  DependentSelects.initialize()
  
  $(document).on "page:before-change", ->
    # Guardar estado previo del sidebar
    window.prevSidebarStatus = {}
    for item in $("#menu li.hasSubmenu > a")
      window.prevSidebarStatus[$(item).attr("href")] = $(item).parent().hasClass("active")
      
  $(document).on "page:change", ->
    # Restaurar el estado previo del Sidebar
    for key, status of window.prevSidebarStatus
      if status
        $(key).addClass("in")
        $(key).siblings("a").removeClass("collapsed")
        $(key).parent().addClass("active") 
      else 
        $(key).removeClass("in")
        $(key).siblings("a").addClass("collapsed")
        $(key).parent().removeClass("active")  

    # UniformJS: Sexy form elements
    if ($('.uniformjs').length) 
      $('.uniformjs').find("select, input, button, textarea").uniform()
      
    # Configuración del Menú
    config_menu()
      
    # DependetSelects
    DependentSelects.initialize()

    # DatePicker
    $('.datepicker').datetimepicker
      pickTime: false
      language: 'es'

      