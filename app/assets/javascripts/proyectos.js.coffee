# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$(document).ready ->
  $(document).on "cocoon:after-insert", '.proyectos #proyecto_financiamiento', (e,item_to_be_added)->
    DependentSelects.initialize(item_to_be_added)

  $(document).on "cocoon:after-insert", '.proyectos #proyecto_ubicacion', (e,item_to_be_added)->
    DependentSelects.initialize(item_to_be_added)
