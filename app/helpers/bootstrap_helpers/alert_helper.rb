# coding: utf-8

# Helpers for generate alert messages using Twitter Bootstrap markup
#
# Copyright 2012 Grupo Event Radar C.A.
# http://www.eventradar.com.ve
# http://metric.la
module BootstrapHelpers
  module AlertHelper
    
    # Generates an alert message with the Twitter Bootstrap markup
    # If +msg+ is not specified, uses the Rails +alert+ helper as message.
    # The option :close => true generates a link to close de message.
    def alert_message(msg = nil, options = {:close => true})
      msg, options = nil, msg if msg.is_a?(Hash)
      msg ||= alert
      
      bootstrap_alert_message(msg, '', options)
    end
    
    # Genera un mensaje de error con los estilos de Bootstrap
    # If +msg+ is not specified, uses the flash[:error] as message.
    # The option :close => true generates a link to close de message.
    def error_message(msg = nil, options = {:close => true})
      msg, options = nil, msg if msg.is_a?(Hash)
      msg ||= flash[:error]
      
      bootstrap_alert_message(msg, 'alert-error', options)
    end
    
    # Generates an information message with the Twitter Bootstrap markup
    # If +msg+ is not specified, uses the flash[:info] as message.
    # The option :close => true generates a link to close de message.
    def info_message(msg = nil, options = {:close => true})
      msg, options = nil, msg if msg.is_a?(Hash)
      msg ||= flash[:info]

      bootstrap_alert_message(msg, 'alert-info', options)
    end
    
    # Generates a notice message with the Twitter Bootstrap markup
    # If +msg+ is not specified, uses the Rails +info+ helper as message.
    # The option :close => true generates a link to close de message.
    def notice_message(msg = nil, options = {:close => true})
      msg, options = nil, msg if msg.is_a?(Hash)
      msg ||= notice

      bootstrap_alert_message(msg, 'alert-success', options)
    end
    
    # Generates an alert message with the Twitter Bootstrap markup
    # +msg+ is the message text to show.
    # +css+ allows to assign the type of message with css classes (alert-error, alert-suscess, alert-info).
    # The option :close => true generates a link to close de message (default).
    def bootstrap_alert_message(msg, css = "", options = {:close => true})
      if msg
        css_classes = "alert fade in"
        css_classes << " #{css}" unless css.blank?
        close_link = options[:close] ? link_to("×", "#", :class => "close", :'data-dismiss' => "alert") : ""
        content_tag :div, close_link.html_safe + msg, :class => css_classes
      end
    end
    
  end
end