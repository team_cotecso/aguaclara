class FuenteFinanciamiento < ActiveRecord::Base
  has_many :fondos_financiamiento, dependent: :destroy
  #has_many :desembolsos_fuentes, class_name: "DesembolsoFuente"
  
  has_many :proyectos_financiamientos, class_name: "ProyectoFinanciamiento", dependent: :destroy

  validates :nombre,
    length: { maximum: 255 },
    presence: true, 
    uniqueness: { case_sensitive: false }
    
  validates_format_of :nombre, 
                      :with    => /^([A-Za-zÁÉÍÓÚÑÜáéíóúñü0-9\s_-])+$/,
                      :message => 'sólo letras, números, - y _',
                      :allow_blank => true,
                      :multiline => true
    
  
  # RailsAdmin config
  rails_admin do
    list do
      field :nombre
    end
    edit do
      field :nombre
    end
  end
end
