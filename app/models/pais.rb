class Pais < ActiveRecord::Base
  self.table_name = "paises"
  validates :codigo, :nombre, :nacionalidad, presence: true
end
