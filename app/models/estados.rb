# Define la máquina de estados para las solicitudes.
module Estados

  def self.included(receiver)
    receiver.extend ClassMethods
    receiver.class_eval do
      include InstanceMethods
      include AASM
      
      # Define la máquina de estados para los proyectos.
       aasm_column :estatus_dinamico

       aasm_initial_state :inicio
       
       aasm_state :inicio
       aasm_state :ejecucion
       aasm_state :inaugurada

       aasm_event :en_ejecucion, :after => Proc.new { notificar_cambio_estatus } do
          transitions :from => :inicio, :to => :ejecucion
       end

       aasm_event :por_inaugurar, :after => Proc.new { notificar_cambio_estatus } do
          transitions :from => :ejecucion, :to => :inaugurada
       end

    end
  end

  module ClassMethods

  end

  module InstanceMethods

    def notificar_cambio_estatus
      # Para Enviar Correo al Cambiar el Estado
      #SolicitanteMailer.pendiente_email(self).deliver
    end
   
  end

end

