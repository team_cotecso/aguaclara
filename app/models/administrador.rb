class Administrador < ActiveRecord::Base
  # Lista de roles
  ROLES = %w[admin]
    
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable,
         :recoverable, :rememberable, :trackable, :validatable
  
         
  has_paper_trail :class_name => 'Version'

  def roles=(roles)
    self.roles_mask = (roles & ROLES).map { |r| 2**ROLES.index(r) }.inject(0, :+)
  end

  
  def roles
    ROLES.reject do |r|
      ((roles_mask || 0) & 2**ROLES.index(r)).zero?
    end
  end
  
  def is?(role)
    roles.include?(role.to_s)
  end

  # Rasil Admin config
  rails_admin do
    list do
      field :email
      field :nombre
    end

    edit do
      field :email
      field :nombre
      field :password
      field :roles do
        partial "administradores/roles_form_fields"
      end
    end
  end  

  def name
    nombre
  end

end
