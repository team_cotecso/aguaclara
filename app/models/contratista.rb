class Contratista < ActiveRecord::Base
  validates :razon_social,
    length: { maximum: 255 },
    presence: true, 
    uniqueness: { case_sensitive: false }
  
  #validates :rif,
  #  length: { maximum: 12 },
  #  format: { with: /\A[VEP]\d{4,8}\z/, allow_blank: true },
  #  presence: true, 
  #  uniqueness: { case_sensitive: false }
  
  validates :direccion, length: { maximum: 255 }
  validates :telefono, length: { maximum: 50 }
  
  has_many :contratos

  # RailsAdmin config
  rails_admin do
    list do
      field :razon_social
      field :rif
      field :direccion
      field :telefono
    end
    edit do
      field :razon_social
      field :rif
      field :direccion
      field :telefono
    end
  end

  def nombre
    return "(#{rif}) #{razon_social}"    
  end
end
