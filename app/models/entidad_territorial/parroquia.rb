class EntidadTerritorial::Parroquia < EntidadTerritorial
  default_scope { order('nombre') } 

  has_many :sectores, foreign_key: "entidad_territorial_id"
  
  belongs_to :municipio,
    class_name: "EntidadTerritorial::Municipio", 
    foreign_key: 'parent_id'
  
  # TODO: Si se usa como atajo hasta estados, Reemplazar por un delegate
  has_one :estado,
    class_name: "EntidadTerritorial::Estado", 
    foreign_key: 'parent_id',
    through: :municipio

  has_many :proyectos_ubicaciones, class_name: "ProyectoUbicacion", dependent: :destroy
  has_many :proyectos, through: :proyectos_ubicaciones

  # RailsAdmin configuration
  rails_admin do
    list do
      field :nombre
      field :municipio
    end
    edit do
      field :nombre
      field :municipio
    end
  end

end