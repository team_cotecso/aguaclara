class EntidadTerritorial::Ciudad < EntidadTerritorial
  default_scope { order('nombre') } 
  
  belongs_to :estado,
    class_name: "EntidadTerritorial::Estado", 
    foreign_key: 'parent_id'


  # RailsAdmin configuration
  rails_admin do
    list do
      field :nombre
      field :estado
    end
    edit do
      field :nombre
      field :estado
    end
  end

end