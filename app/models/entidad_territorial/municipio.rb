class EntidadTerritorial::Municipio < EntidadTerritorial
  default_scope { order('nombre') } 
  
  has_many :parroquias, 
    class_name: "EntidadTerritorial::Parroquia", 
    foreign_key: 'parent_id'
  
  belongs_to :estado,
    class_name: "EntidadTerritorial::Estado", 
    foreign_key: 'parent_id'

  has_many :proyectos_ubicaciones, class_name: "ProyectoUbicacion", dependent: :destroy
  has_many :proyectos, through: :proyectos_ubicaciones

  # RailsAdmin configuration
  rails_admin do
    list do
      field :nombre
      field :estado
    end
    edit do
      field :nombre
      field :estado
    end
  end

  def descendientes
    [self.id] + children.collect(&:descendientes).flatten
  end

end