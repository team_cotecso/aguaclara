class EntidadTerritorial::Estado < EntidadTerritorial

  default_scope { order('nombre') } 

  has_many :ciudades, 
    class_name: "EntidadTerritorial::Ciudad", 
    foreign_key: 'parent_id'
  
  has_many :municipios, 
    class_name: "EntidadTerritorial::Municipio", 
    foreign_key: 'parent_id'

  has_many :parroquias, :through => :municipios,
    class_name: "EntidadTerritorial::Estado", 
    foreign_key: 'parent_id'

  has_many :proyectos_ubicaciones, class_name: "ProyectoUbicacion", dependent: :destroy
  has_many :proyectos, through: :proyectos_ubicaciones


  # RailsAdmin configuration
  rails_admin do
    list do
      field :nombre
    end
    edit do
      field :nombre
    end
  end

end