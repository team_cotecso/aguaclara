class MontoProyecto < ActiveRecord::Base
  self.table_name = "montos_proyectos"
  validates :solicitado,
    numericality: { greater_than: 0, allow_blank: false },
    presence: true
  validates :aprobado,
    numericality: { greater_than: 0, allow_blank: false },
    presence: true

  validates :moneda_id, presence: true  
  

  belongs_to :proyecto
  belongs_to :moneda

end
