class MontoValuacion < ActiveRecord::Base
  validates :monto,
    numericality: { greater_than: 0, allow_blank: true },
    presence: true

  belongs_to :valuacion
  belongs_to :desembolso_fuente
  belongs_to :moneda

  scope :por_proyecto, ->(proyecto_id) {where("proyectos.id = ?",proyecto_id).joins(valuacion: {contrato: :proyecto})}


end
