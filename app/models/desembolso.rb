class Desembolso < ActiveRecord::Base
  validates :fecha,
    presence: true
  validates :observacion,
    length: { maximum: 255 }
  validates :monto,
    numericality: { greater_than: 0, allow_blank: true },
    presence: false

  validates :estatus_transferencia, :valuacion,
  presence: true


  belongs_to :proyecto
  belongs_to :estatus_transferencia, class_name: "EstatusTransferencia", foreign_key: "estatus_id"
  
  has_many :desembolsos_fuentes, class_name: "DesembolsoFuente", dependent: :destroy
  #has_many :fuentes_financiamiento, through: :desembolsos_fuentes

  accepts_nested_attributes_for :desembolsos_fuentes, reject_if: :all_blank, allow_destroy: true

   scope :por_proyecto, ->(proyecto_id) {where("proyectos.id = ?",proyecto_id).joins(contrato: :proyecto)}

  def montos
    return desembolsos_fuentes.map{|d| "#{d.monto} #{d.moneda.simbolo}"}.join(", ") unless desembolsos_fuentes.nil?
  end

  def self.datos_grafico_desembolsos
    datos = []
    EstatusTransferencia.all.each do |e|
      datos.push([e.nombre, e.desembolsos.count])
    end
    datos
  end    
end
