class Actividad < ActiveRecord::Base

  after_save :verificar_estado_proyecto

  belongs_to :tipo_actividad
  belongs_to :proyecto

  validate :maximo_peso

  validates :descripcion,
    length: { maximum: 255 },
    presence: true

  validates :peso,
    numericality: { only_integer: true, greater_than_or_equal_to: 0, less_than_or_equal_to: 100 }, 
    presence: true

  validates :porcentaje_ejecucion, 
    numericality: { only_integer: true, greater_than_or_equal_to: 0, less_than_or_equal_to: 100 }, 
    presence: true

  validates :fecha_inicial, 
    presence: true

  validates :fecha_final,
    presence: true

  validates :observaciones,
    length: { maximum: 255 }

  validates :proyecto, presence: true
  validates :tipo_actividad, presence: true

  def porcentaje_global
    ((porcentaje_ejecucion * peso) / 100).round(2)
  end

  def self.search(query)
    result = self.all
    if !query["tipo_actividad_id"].blank? 
      result = result.where("tipo_actividad_id = ?", query["tipo_actividad_id"])
    end
    result
  end

private

  def maximo_peso
    if new_record?
      suma_pesos = proyecto.actividades.inject(0){|sum,e| sum += e.peso} + peso
    else
      suma_pesos = proyecto.actividades.where("id <> #{id}").inject(0){|sum,e| sum += e.peso} + peso
    end
    if suma_pesos > 100
      errors.add(:peso, "El peso de todas las actividades del proyecto es mayor a 100")
    end
  end

  def verificar_estado_proyecto
    
    ejecutado = proyecto.actividades.sum("(porcentaje_ejecucion * peso) / 100").round(2)

    proyecto.comentarios = "Cambio de Estado"
    proyecto.en_ejecucion! if ejecutado > 0.0 and ejecutado < 100.0
    proyecto.por_inaugurar! if ejecutado >= 100.0

  end


end