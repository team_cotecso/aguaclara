class Bitacora < ActiveRecord::Base
  
  has_attached_file :file
  #validates_attachment_content_type :avatar, :content_type => /\Aimage\/.*\Z/

  belongs_to :proyecto
  belongs_to :usuario, class_name: 'Administrador'

  validates :titulo, :proyecto, :usuario, presence: true
  validates_attachment :file, :size => { :in => 0..1000.kilobytes }
end