class EnteEjecutor < ActiveRecord::Base
  validates :nombre,
    length: { maximum: 255 },
    presence: true, 
    uniqueness: { case_sensitive: false }

  validates_format_of :nombre, 
                      :with    => /^([A-Za-zÁÉÍÓÚÑÜáéíóúñü0-9\s_-])+$/,
                      :message => 'sólo letras, números, - y _',
                      :allow_blank => true,
                      :multiline => true
  
  validates_presence_of :institucion_id
    
  belongs_to :institucion
  has_many :proyectos
  
  # RailsAdmin config
  rails_admin do
    list do
      field :nombre
    end
    edit do
      field :nombre
      field :institucion
    end
  end
end
