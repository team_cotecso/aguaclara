class Moneda < ActiveRecord::Base
  validates :valor,
    numericality: { greater_than: 0, allow_blank: false },
    presence: true
  validates :nombre,
    length: { maximum: 255 },
    presence: true
  validates :simbolo,
    length: { maximum: 10 },
    presence: true

  has_many :montos_proyectos, class_name: "MontoProyecto"
  has_many :montos_valuaciones, class_name: "MontoValuacion"
  has_many :desembolsos_fuentes, class_name: "DesembolsoFuente"

  # RailsAdmin config
  rails_admin do
    list do
      field :nombre
      field :simbolo
      field :valor
      field :aumenta
      field :es_principal
    end

    edit do
      field :nombre
      field :simbolo
      field :valor
      field :aumenta
      field :es_principal
    end
  end

  def principal
    Moneda.where(es_principal: true).first
  end

end