class Proyecto < ActiveRecord::Base
  include Estados 
  
  attr_accessor :comentarios
  validates :nombre,
    length: { maximum: 255 },
    presence: true
  validates :descripcion,
    length: { maximum: 255 },
    presence: true
  validates :punto_cuenta,
    length: { maximum: 20 },
    presence: true
  validates :lapso_ejecucion_en_meses,
    numericality: { only_integer: true, greater_than: 0, allow_blank: true },
    presence: true
  validates :habitantes_beneficiados,
    numericality: { only_integer: true, greater_than_or_equal_to: 0, allow_blank: true },
    presence: true
  validates :empleos_directos,
    numericality: { only_integer: true, greater_than_or_equal_to: 0, allow_blank: true },
    presence: true
  validates :empleos_indirectos,
    numericality: { only_integer: true, greater_than_or_equal_to: 0, allow_blank: true },
    presence: true  
  validates :monto_solicitado,
    numericality: { greater_than: 0, allow_blank: true },
    presence: false
  validates :monto_aprobado,
    numericality: { greater_than: 0, allow_blank: true },
    presence: false
  validates :numero_proyecto,
    length: { maximum: 15 },
    presence: true
  validates_presence_of :fecha_inicial, :fecha_final,
    :unidad, :ente_ejecutor, :institucion, :estatus3i, :tipo_obra,
    :sub_tipo_obra, :punto_cuenta
  validates_presence_of :comentarios, :on => :update
  
  validate :fechas

  validates :proyectos_financiamientos, :presence => true
  validates :montos_proyectos, :presence => true

  belongs_to :unidad
  belongs_to :ente_ejecutor
  belongs_to :institucion
  belongs_to :estatus3i, class_name: "Estatus3i", foreign_key: "estatus_id"
  belongs_to :tipo_obra
  belongs_to :sub_tipo_obra
  
  has_many :contratos, dependent: :destroy, inverse_of: :proyecto
  has_many :contratistas, through: :contratos
  has_many :actividades
  has_many :sectores, through: :proyectos_ubicaciones 
  has_many :estados, through: :proyectos_ubicaciones
  has_many :municipios, through: :proyectos_ubicaciones
  has_many :parroquias, through: :proyectos_ubicaciones
  has_many :desembolsos
  has_many :desembolsos_fuentes, through: :desembolsos
  has_many :bitacoras
  has_many :proyectos_financiamientos, class_name: "ProyectoFinanciamiento", dependent: :destroy
  has_many :proyectos_ubicaciones, class_name: "ProyectoUbicacion", dependent: :destroy
  has_many :montos_proyectos, class_name: "MontoProyecto", dependent: :destroy
  
  has_many :monedas, through: :montos_proyectos
  has_many :valuaciones, through: :contratos

  accepts_nested_attributes_for :proyectos_financiamientos, reject_if: :all_blank, allow_destroy: true
  accepts_nested_attributes_for :proyectos_ubicaciones, reject_if: :all_blank, allow_destroy: true
  accepts_nested_attributes_for :montos_proyectos, reject_if: :all_blank, allow_destroy: true
  
  scope :ente_ejecutor, ->(ente_ejecutor) {joins(:ente_ejecutor).where("entes_ejecutores.id = ?",ente_ejecutor)}
  scope :fuente_financiamiento, ->(fuente_financiamiento) {joins(:proyectos_financiamientos).where("proyectos_financiamientos.fuente_financiamiento_id = ?",fuente_financiamiento)}
  scope :fondo_financiamiento, ->(fondo_financiamiento) {joins(:proyectos_financiamientos).where("proyectos_financiamientos.fondo_financiamiento_id = ?",fondo_financiamiento)}
  scope :por_estado, ->(estado_id) {joins(:proyectos_ubicaciones).where("proyectos_ubicaciones.estado_id = ?",estado_id)}
  scope :por_municipio, ->(municipio_id) {joins(:proyectos_ubicaciones).where("proyectos_ubicaciones.municipio_id = ?",municipio_id)}
  scope :por_parroquia, ->(parroquia_id) {joins(:proyectos_ubicaciones).where("proyectos_ubicaciones.parroquia_id = ?",parroquia_id)}
  scope :por_sector, ->(sector_id) {joins(:proyectos_ubicaciones).where("proyectos_ubicaciones.sector_id = ?",sector_id)}
  scope :tipo_obra, ->(tipo_obra) {joins(:tipo_obra).where(s"tipos_obras.id = ?",tipo_obra)}
  scope :sub_tipo_obra, ->(sub_tipo_obra) {joins(:sub_tipo_obra).where("sub_tipos_obras.id = ?",sub_tipo_obra)}
  scope :estatus_3i, ->(estatus_3i) {where("estatus_id = ?",estatus_3i)}
  scope :punto_cuenta, ->(punto_cuenta) {where("punto_cuenta = ?",punto_cuenta)}
  scope :estatus_transferencia, ->(estatus_transferencia) {joins(:desembolso).where("desembolsos.estatus_id = ?",estatus_transferencia)}
  scope :por_proyecto, ->(proyecto) {where("lower(nombre) like ?","%#{proyecto.downcase}%")}
  scope :numero_proyecto, ->(numero_proyecto) {where("lower(numero_proyecto) like ?","%#{numero_proyecto.downcase}%")}
  scope :unidad, ->(unidad) {joins(:unidad).where("unidades.id = ?",unidad)}
  scope :impacto, ->(impacto) {where("impacto = ?",impacto)}
  scope :posee_recurso, ->(posee_recurso) {where("posee_recursos = ?",posee_recurso)}
  scope :fecha_inicial, ->(fecha_inicial) {where("fecha_inicial >= ?",fecha_inicial)}
  scope :fecha_final, ->(fecha_final) {where("fecha_final <= ?",fecha_final)}
  scope :institucion, ->(institucion) {joins(:institucion).where("instituciones.id = ?",institucion)}  
  scope :iniciadas, ->{where(estatus_dinamico: "inicio")}
  scope :ejecutandose, ->{where(estatus_dinamico: "ejecucion")}
  scope :inauguradas, ->{where(estatus_dinamico: "inaugurada")}
  scope :listado_desembolsos, ->(sub_tipo_obra) {joins(:sub_tipo_obra).where("sub_tipos_obras.id = ?",sub_tipo_obra)}




  def self.search_by_query(query)
    result = self.all
    if !query["ente_ejecutor"].nil? && !query["ente_ejecutor"].empty?
      result = result.ente_ejecutor(query["ente_ejecutor"])
    end
    if !query["fuente_financiamiento"].nil? && !query["fuente_financiamiento"].to_s.empty?
      result = result.fuente_financiamiento(query["fuente_financiamiento"])
    end
    if !query["fondo_financiamiento"].nil? && !query["fondo_financiamiento"].to_s.empty?
      result = result.fondo_financiamiento(query["fondo_financiamiento"])
    end
    if !query["tipo_obra"].nil? && !query["tipo_obra"].to_s.empty?
      result = result.tipo_obra(query["tipo_obra"])
    end
    if !query["sub_tipo_obra"].nil? && !query["sub_tipo_obra"].to_s.empty?
      result = result.sub_tipo_obra(query["sub_tipo_obra"])
    end
    if !query["estatus_3i"].nil? && !query["estatus_3i"].to_s.empty?
      result = result.estatus_3i(query["estatus_3i"])
    end
    if !query["estatus_transferencia"].nil? && !query["estatus_transferencia"].to_s.empty?
      result = result.estatus_transferencia(query["estatus_transferencia"])
    end
    if !query["proyecto"].nil? && !query["proyecto"].to_s.empty?
      result = result.por_proyecto(query["proyecto"])
    end
    if !query["nombre"].nil? && !query["nombre"].to_s.empty?
      result = result.por_proyecto(query["nombre"])
    end
    if !query["numero_proyecto"].nil? && !query["numero_proyecto"].to_s.empty?
      result = result.numero_proyecto(query["numero_proyecto"])
    end
    if !query["punto_cuenta"].nil? && !query["punto_cuenta"].to_s.empty?
      result = result.punto_cuenta(query["punto_cuenta"])
    end
    if !query["unidad"].nil? && !query["unidad"].to_s.empty?
      result = result.unidad(query["unidad"])
    end
    if !query["impacto"].nil? && !query["impacto"].to_s.empty?
      result = result.impacto(query["impacto"])
    end
    if !query["posee_recurso"].nil? && !query["posee_recurso"].to_s.empty?
      result = result.posee_recurso(query["posee_recurso"])
    end
    if !query["institucion"].nil? && !query["institucion"].to_s.empty?
      result = result.institucion(query["institucion"])
    end
    if !query["fecha_inicial"].nil? && !query["fecha_inicial"].to_s.empty?
      result = result.fecha_inicial(query["fecha_inicial"])
    end
    if !query["fecha_final"].nil? && !query["fecha_final"].to_s.empty?
      result = result.fecha_final(query["fecha_final"])
    end
    if !query["estado_id"].nil? && !query["estado_id"].to_s.empty?
      result = result.por_estado(query["estado_id"])
    end
    if !query["municipio_id"].nil? && !query["municipio_id"].to_s.empty?
      result = result.por_municipio(query["municipio_id"])
    end
    if !query["parroquia_id"].nil? && !query["parroquia_id"].to_s.empty?
      result = result.por_parroquia(query["parroquia_id"])
    end
    if !query["sector_id"].nil? && !query["sector_id"].to_s.empty?
      result = result.por_sector(query["sector_id"])
    end
    result
  end
 
  def porcentaje_avance_fisico
    ejecucion_fisica
  end
  
  def porcentaje_avance_financiero
    ejecucion_financiera
  end

  rails_admin do
    list do
      field :nombre
      field :descripcion
      field :fecha_inicial
      field :fecha_final
    end
  end
 

  def ejecucion_fisica
    actividades.inject(0){|sum,e| sum += e.porcentaje_global}.round(2)
  end

  def ejecucion_financiera
    ta = total_aprobado
    tv = total_valuaciones
    if ta > 0.0 
      ((tv * 100) / ta).round(2)
    else
      0.0
    end
  end

  def total_aprobado
    total = 0
    montos_proyectos.includes(:moneda).each do |m|
      total += m.aprobado * m.moneda.valor if m.moneda.aumenta?
      total += m.aprobado / m.moneda.valor unless m.moneda.aumenta?
    end
    total.round(2)
  end

  def total_valuaciones
    total = 0
    MontoValuacion.por_proyecto(id).includes(:moneda).each do |m|
      total += m.monto * m.moneda.valor if m.moneda.aumenta?
      total += m.monto / m.moneda.valor unless m.moneda.aumenta?
    end
    total.round(2)
  end

  def pesos
    actividades.inject(0){|sum,e| sum += e.peso}
  end

  def montos_solicitados
    return montos_proyectos.map{|d| "#{d.solicitado} #{d.moneda.simbolo}"}.join(", ") unless montos_proyectos.nil?
  end

  def montos_aprobados
    return montos_proyectos.map{|d| "#{d.aprobado} #{d.moneda.simbolo}"}.join(", ") unless montos_proyectos.nil?
  end

  def self.datos_grafico_ejecucion_financiera
    datos = []
    Moneda.all.each do |m|
      datos.push({name: m.simbolo, data: [
          m.montos_proyectos.sum(:solicitado),
          m.montos_proyectos.sum(:aprobado),
          m.desembolsos_fuentes.sum(:monto),
          m.montos_valuaciones.sum(:monto)
        ]})
    end
    datos
  end

  def self.datos_grafico_proyectos
    [
      ['Iniciada',   Proyecto.iniciadas.count],
      ['En Ejecución', Proyecto.ejecutandose.count],
      ['Inauguradas', Proyecto.inauguradas.count],
    ]
  end
  
private

  # Valida que la fecha final no sea menor que la fecha de inicio del proyecto
  def fechas
    if self.fecha_final && self.fecha_inicial
      if self.fecha_final < self.fecha_inicial
        errors.add :fecha_final, 'debe ser mayor que la fecha de inicio'
      end
    end
  end
  
end