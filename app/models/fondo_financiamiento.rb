class FondoFinanciamiento < ActiveRecord::Base
  belongs_to :fuente_financiamiento

  has_many :proyectos_financiamientos, class_name: "ProyectoFinanciamiento", dependent: :destroy

  has_many :desembolsos_fuentes, through: :proyectos_financiamientos
  has_many :proyectos, through: :proyectos_financiamientos

   validates :nombre,
    length: { maximum: 255 },
    presence: true, 
    uniqueness: { case_sensitive: false, scope: :fuente_financiamiento_id }
    
  validates_format_of :nombre, 
                      :with    => /^([A-Za-zÁÉÍÓÚÑÜáéíóúñü0-9\s_-])+$/,
                      :message => 'sólo letras, números, - y _',
                      :allow_blank => true,
                      :multiline => true
    
  validates_presence_of :fuente_financiamiento_id

  # RailsAdmin config
  rails_admin do
    list do
      field :nombre
    end
    edit do
      field :nombre
      field :fuente_financiamiento
    end
  end

  def self.datos_grafico_fondos
    datos = []
    FondoFinanciamiento.all.each do |f|
      datos.push([f.nombre, f.proyectos.count])
    end
    datos
  end

end
