class DesembolsoFuente < ActiveRecord::Base
  validates :monto,
    numericality: { greater_than: 0, allow_blank: true },
    presence: true
  
  belongs_to :desembolso
  belongs_to :moneda
  belongs_to :proyecto_financiamiento
  has_many :montos_valuaciones

  scope :por_proyecto, ->(proyecto) {joins(:desembolso).where(desembolsos: {proyecto_id: proyecto})}

  def nombre
    return "#{proyecto_financiamiento.nombre} - (#{monto} #{moneda.simbolo})" unless proyecto_financiamiento.nil?
  end


end
