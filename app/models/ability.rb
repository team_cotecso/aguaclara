# Definición de habilidades de cada rol
# para los Administradores
class Ability
  include CanCan::Ability
  
  # user es una instancia de Administrador
  def initialize(user)
    # TODO: Cambiar: Esto es para evitar quedarse sin acceso 
    # mientras se terminan de definir las habilidades de cada rol.
    can :access, :rails_admin # Todos loas admini deben ingresar a rails admin
    can :dashboard # Todos pueden ingresar al dashboard?
    
    # Ver detalles de accesibilidad de Rails Admin en:
    # https://github.com/sferik/rails_admin/wiki/CanCan
    
    # Par controladores no Rails Admin, usar 
    # En el controlador load_and_authorize_resource
    # Detalles en: https://github.com/ryanb/cancan/wiki/authorizing-controller-actions
    
    if user && user.is?(:admin)
      # Habilidades específicas de Administrador
      
      ########
      # BORRAR si el rol Admin no tiene acceso a todo!!!!
      ########
      can :manage, :all
      
      # Este caso es el tipico, el controlador es el mismo que el modelo,
      # y adicionalmente es manejado por RailsAdmin
    end
    
    if user && user.is?(:digitalizador)
      # Habilidades específicas de Digitalizador de la deppa
      # Falta Módulo de Carga de Expediente de Papiro
      
    end

  end
  
end