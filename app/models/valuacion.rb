class Valuacion < ActiveRecord::Base

  # Asociaciones
  belongs_to :contrato
  has_many :montos_valuaciones, class_name: "MontoValuacion", dependent: :destroy

  accepts_nested_attributes_for :montos_valuaciones, reject_if: :all_blank, allow_destroy: true 
 
  # Validaciones
  validates :ingeniero_inspector, presence: true, length: {maximum: 100}
  validates :numero, :fecha_desde, :fecha_hasta, :contrato, presence: true
  validates :numero, uniqueness: :numero

  validates :montos_valuaciones, :presence => true

  # Scopes
  scope :por_numero_contrato, ->(numero) {where("lower(contratos.numero) like ?","%#{numero.downcase}%").joins(:contrato)}
  scope :por_nombre_proyecto, ->(nombre) {where("lower(proyectos.nombre) like ?","%#{nombre.downcase}%").joins(contrato: :proyecto)}
  scope :por_proyecto, ->(proyecto_id) {where("proyectos.id = ?",proyecto_id).joins(contrato: :proyecto)}


  class << self
    def search_by_query(query)
      result = self.all
      if !query["numero_contrato"].nil? && !query["numero_contrato"].to_s.empty?
        result = result.por_numero_contrato(query["numero_contrato"])
      end
      if !query["numero_valuacion"].nil? && !query["numero_valuacion"].to_s.empty?
        result = result.where(numero: query["numero_valuacion"])
      end
      if !query["ingeniero_inspector"].nil? && !query["ingeniero_inspector"].to_s.empty?
        result = result.where(ingeniero_inspector: query["ingeniero_inspector"])
      end      
      if !query["nombre_proyecto"].nil? && !query["nombre_proyecto"].to_s.empty?
        result = result.por_nombre_proyecto(query["nombre_proyecto"])
      end 
      if !query["desde"].nil? && !query["desde"].to_s.empty?
        result = result.where("fecha >= ?", query["desde"])
      end  
      if !query["hasta"].nil? && !query["hasta"].to_s.empty?
        result = result.where("fecha <= ?", query["hasta"])
      end  
      result
    end
  end

  def montos
    return montos_valuaciones.map{|d| "#{d.monto} #{d.moneda.simbolo}"}.join(", ") unless montos_valuaciones.nil?    
  end
  
end