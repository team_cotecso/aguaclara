class Estatus3i < Estatus
  has_many :proyectos, foreign_key: "estatus_id"
  
  # RailsAdmin config
  rails_admin do
    list do
      field :nombre
    end
    edit do
      field :nombre
    end
  end
end