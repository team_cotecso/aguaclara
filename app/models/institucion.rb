class Institucion < ActiveRecord::Base
  validates :nombre,
    length: { maximum: 255 },
    presence: true, 
    uniqueness: { case_sensitive: false }

  validates_format_of :nombre, 
                      :with    => /^([A-Za-zÁÉÍÓÚÑÜáéíóúñü0-9\s_-])+$/,
                      :message => 'sólo letras, números, - y _',
                      :allow_blank => true,
                      :multiline => true  
    
  validates_presence_of :unidad_id
  
  belongs_to :unidad
  has_many :proyectos
  has_many :entes_ejecutores
  
  # RailsAdmin config
  rails_admin do
    list do
      field :nombre
      field :unidad
    end
    edit do
      field :nombre
      field :unidad
    end
  end
end
