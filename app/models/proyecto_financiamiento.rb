class ProyectoFinanciamiento < ActiveRecord::Base
  belongs_to :proyecto
  belongs_to :fuente_financiamiento
  belongs_to :fondo_financiamiento

  has_many :desembolsos_fuentes, class_name: 'DesembolsoFuente', foreign_key: 'proyecto_financiamiento_id'

  validates :fuente_financiamiento, :fondo_financiamiento, presence: true


  def nombre
    return "#{fuente_financiamiento.nombre} - #{fondo_financiamiento.nombre}" unless fuente_financiamiento.nil? or fondo_financiamiento.nil?
  end

  rails_admin do
    list do
      field :proyecto
      field :fuente_financiamiento
      field :fondo_financiamiento
    end
  end

end
