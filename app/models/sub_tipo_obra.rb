class SubTipoObra < ActiveRecord::Base
  has_many :proyectos
  
  validates :nombre,
    length: { maximum: 255 },
    presence: true, 
    uniqueness: { case_sensitive: false }
  validates_format_of :nombre, 
                      :with    => /^([A-Za-zÁÉÍÓÚÑÜáéíóúñü0-9\s_-])+$/,
                      :message => 'sólo letras, números, - y _',
                      :allow_blank => true,
                      :multiline => true

  validates_presence_of :tipo_obra_id
  
  belongs_to :tipo_obra
  
  # RailsAdmin config
  rails_admin do
    list do
      field :nombre
    end
    edit do
      field :nombre
      field :tipo_obra
    end
  end
end
