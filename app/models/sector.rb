class Sector < ActiveRecord::Base
  has_many :proyectos, through: :proyectos_ubicaciones
  has_many :proyectos_ubicaciones, class_name: "ProyectoUbicacion", dependent: :destroy
  belongs_to :parroquia, class_name: 'EntidadTerritorial::Parroquia', foreign_key: "entidad_territorial_id"

  validates :nombre,
    length: { maximum: 255 },
    presence: true

  validates :poblacion,
    numericality: { only_integer: true }, 
    presence: true

  validates :parroquia, presence: true

  # RailsAdmin config
  rails_admin do
    list do
      field :nombre
      field :poblacion
    end

    edit do
      field :nombre
      field :poblacion
      field :entidad_territorial_id do
        partial "administradores/entidad_territorial_form_fields"
      end
    end
  end



end
