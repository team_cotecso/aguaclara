class TipoObra < ActiveRecord::Base
  has_many :proyectos
  
  validates :nombre,
    length: { maximum: 255 },
    presence: true, 
    uniqueness: { case_sensitive: false }
    
  validates_format_of :nombre, 
                      :with    => /^([A-Za-zÁÉÍÓÚÑÜáéíóúñü0-9\s_-])+$/,
                      :message => 'sólo letras, números, - y _',
                      :allow_blank => true,
                      :multiline => true
    
  has_many :sub_tipos_obras, class_name: "SubTipoObra", :dependent => :destroy
  
  # RailsAdmin config
  rails_admin do
    list do
      field :nombre
    end
    edit do
      field :nombre
    end
  end
end
