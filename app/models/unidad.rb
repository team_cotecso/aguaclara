class Unidad < ActiveRecord::Base
  validates :nombre,
    length: { maximum: 255 },
    presence: true, 
    uniqueness: { case_sensitive: false }

  validates_format_of :nombre, 
                      :with    => /^([A-Za-zÁÉÍÓÚÑÜáéíóúñü0-9\s_-])+$/,
                      :message => 'sólo letras, números, - y _',
                      :allow_blank => true,
                      :multiline => true
    
  has_many :instituciones, :dependent => :destroy
  has_many :entes_ejecutores, :through => :instituciones
  has_many :proyectos
  
  # RailsAdmin config
  rails_admin do
    list do
      field :nombre
    end
    edit do
      field :nombre
    end
  end
end
