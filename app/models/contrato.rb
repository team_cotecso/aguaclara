class Contrato < ActiveRecord::Base
  validates :numero, presence: true, length: { maximum: 255 }
  validates :ingeniero_residente, presence: true, length: { maximum: 255 }
  
  belongs_to :proyecto
  belongs_to :contratista
  
  has_many :valuaciones


  delegate :nombre, to: :proyecto, prefix: true

  validates :numero,
    numericality: {greater_than: 0 }, 
    presence: true

  validates :ingeniero_residente,
    length: { maximum: 255 },
    presence: true

  validates :descripcion,
    length: { maximum: 255 },
    presence: true

  validates :proyecto, presence: true
  validates :contratista, presence: true


  def self.search(params)
    contratos = self.all
    contratos = contratos.where("proyecto_id = ?", params[:proyecto_id]) unless params[:proyecto_id].blank?
    contratos = contratos.where("contratista_id = ?", params[:contrato][:contratista_id]) unless params[:contrato][:contratista_id].blank?
    contratos = contratos.where("lower(proyectos.nombre) like ?", "%#{params[:contrato][:proyecto].downcase}%").joins(:proyecto) unless params[:contrato][:proyecto].blank?
    contratos = contratos.where("lower(contratos.numero) like ?", "%#{params[:contrato][:numero].downcase}%") unless params[:contrato][:numero].blank?
    contratos = contratos.where("lower(contratos.ingeniero_residente) like ?", "%#{params[:contrato][:ingeniero_residente].downcase}%") unless params[:contrato][:ingeniero_residente].blank?
    contratos
  end
  
end
