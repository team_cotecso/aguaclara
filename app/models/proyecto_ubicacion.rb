class ProyectoUbicacion < ActiveRecord::Base
	belongs_to :proyecto, class_name: 'Proyecto', foreign_key: 'proyecto_id'
	belongs_to :estado, class_name: 'EntidadTerritorial::Estado', foreign_key: 'estado_id'
	belongs_to :municipio, class_name: 'EntidadTerritorial::Municipio', foreign_key: 'municipio_id'
	belongs_to :parroquia, class_name: 'EntidadTerritorial::Parroquia', foreign_key: 'parroquia_id'
	belongs_to :sector, class_name: 'Sector', foreign_key: 'sector_id'
end
