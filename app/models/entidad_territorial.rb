class EntidadTerritorial < ActiveRecord::Base
  self.inheritance_column = "tipo"
  
  validates :nombre, 
    presence: true, 
    uniqueness: { scope: [:parent_id, :tipo], case_sensitive: false }
end
