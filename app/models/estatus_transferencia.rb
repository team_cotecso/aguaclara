class EstatusTransferencia < Estatus
  has_many :desembolsos, foreign_key: "estatus_id"
  
  # RailsAdmin config
  rails_admin do
    list do
      field :nombre
    end
    edit do
      field :nombre
    end
  end
end