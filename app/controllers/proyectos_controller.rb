class ProyectosController < ApplicationController
  before_action :configurar_listas, only: [:new, :create, :edit, :update] 
  before_action :buscar_proyecto, only: [:edit, :update, :destroy, :show, :pdf]
  before_filter :buscar_fuentes_financiamientos
  before_filter :buscar_fondos_financiamientos


  # GET /proyectos
  def index
    @proyectos = Proyecto.all
  end

  # GET /proyectos/new
  def new
    @proyecto = Proyecto.new
  end



  def pdf
    render :pdf => 'proyecto', :layout => 'pdf.html'    
  end

  # POST /proyectos
  def create
    @proyecto = Proyecto.new proyecto_params
    if @proyecto.save
      redirect_to proyectos_url, notice: 'El proyecto se ha creado satisfactoriamente.'
    else      
      render action: 'new'
    end
  end

  # GET /proyectos/:id/edit
  def edit
  end

  # PUT /proyectos/:id
  # PATCH /proyectos/:id
  def update
    if @proyecto.update proyecto_params
      bitacora = Bitacora.new(:titulo =>"Actualización del proyecto #{@proyecto.nombre}", :observaciones => @proyecto.comentarios, :proyecto_id => @proyecto.id)
      bitacora.usuario = current_user
      bitacora.save
      logger.info bitacora.inspect
      redirect_to proyectos_url, notice: 'El proyecto ha sido actualizado satisfactoriamente.'
    else
      render action: 'edit'
    end
  end

  # DELETE /proyectos/:id
  def destroy
    if @proyecto.destroy
      redirect_to proyectos_url, notice: 'El proyecto ha sido actualizado satisfactoriamente.'
    else
      redirect_to proyectos_url, notice: 'El proyecto ha sido actualizado satisfactoriamente.'
    end      
  end

  # GET /proyectos/search
  def search
    @proyectos = Proyecto.search_by_query(params[:search])
    render :index
  end

private

  # Busca el proyecto especificado por +params[:id]+
  # Llamado como before_action
  def buscar_proyecto
    @proyecto = Proyecto.find params[:id]
  end

  # Configura las variables de instancias para cargar
  # los diferentes +selects+ del formulario
  def configurar_listas
    @unidades = Unidad.order(:nombre)
    @estatus3i = Estatus3i.all
    @tipos_obras = TipoObra.order(:nombre)
    @estados = EntidadTerritorial::Estado.order(:nombre)
    @municipios = EntidadTerritorial::Municipio.order(:nombre)
    @parroquias = EntidadTerritorial::Parroquia.order(:nombre)
    @sectores = Sector.order(:nombre)
  end
  def buscar_fondos_financiamientos
    @fondos_financiamientos = FondoFinanciamiento.all
  end
  def buscar_fuentes_financiamientos
    @fuentes_financiamientos = FuenteFinanciamiento.all
  end

  def proyecto_params
    params[:proyecto].permit(:nombre,:descripcion,:fecha_inicial, :fecha_final, :lapso_ejecucion_en_meses, :monto_solicitado, :monto_aprobado,
      :unidad_id, :ente_ejecutor_id, :institucion_id, :estatus_id, :impacto, :punto_cuenta, :tipo_obra_id, :sub_tipo_obra_id, :numero_proyecto, :comentarios, :posee_recursos, :habitantes_beneficiados, :empleos_directos, :empleos_indirectos, 
      proyectos_financiamientos_attributes: [:id, :fuente_financiamiento_id, :fondo_financiamiento_id, :_destroy], 
      proyectos_ubicaciones_attributes: [:id, :proyecto_id, :estado_id, :municipio_id, :parroquia_id, :sector_id, :_destroy],
      montos_proyectos_attributes: [:id, :moneda_id, :solicitado, :aprobado, :_destroy])
  end
end
