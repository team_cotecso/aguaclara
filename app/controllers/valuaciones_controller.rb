class ValuacionesController < ApplicationController
  before_action :buscar_contrato, except: [:list, :search]
  before_action :buscar_valuacion, only: [:edit, :update, :destroy]

  # GET /valuaciones
  def index
    @valuaciones = @contrato.valuaciones
  end

  # GET /list_valuaciones
  def list
    @valuaciones = Valuacion.order("fecha_desde DESC")
  end  

  # GET /valuaciones/new
  def new
    @valuacion = @contrato.valuaciones.new 
  end

  # POST /valuaciones
  def create
    @valuacion = @contrato.valuaciones.new valuacion_params
    if @valuacion.save
      redirect_to valuaciones_url, notice: 'La valuación se ha creado satisfactoriamente.'
    else
      render action: 'new'
    end
  end

  # GET /valuaciones/:id/edit
  def edit
  end

  # PUT /valuaciones/:id
  # PATCH /valuaciones/:id
  def update
    if @valuacion.update valuacion_params
      redirect_to valuaciones_url, notice: 'La valuación ha sido actualizado satisfactoriamente.'
    else
      render action: 'edit'
    end
  end

  # DELETE /valuaciones/:id
  def destroy
    if @valuacion.destroy
      redirect_to valuaciones_url, notice: "La valuación número: #{@valuacion.numero} ha sido eliminada satisfactoriamente."
    else      
      redirect_to valuaciones_url, notice: 'Ha ocurrido un error al eliminar la valuación.<br/>'+@valuacion.errors.full_messages
    end      
  end 

  # GET /valuaciones/search
  def search 
    @valuaciones = Valuacion.search_by_query(params[:valuacion])
    if params[:contrato_id]
      @contrato = Contrato.find params[:contrato_id]
      @proyecto = @contrato.proyecto      
      render :index    
    else
      render :list    
    end
  end
private

  # Busca el contrato especificado por +params[:contrato_id]+
  # Llamado como before_action
  def buscar_contrato
    @contrato = Contrato.find params[:contrato_id]
    @proyecto = @contrato.proyecto

    @desembolsosfuentes = DesembolsoFuente.por_proyecto(@proyecto.id)
  end

  # Busca la valuación especificada por +params[:id]+
  # Llamado como before_action
  def buscar_valuacion
    @valuacion = @contrato.valuaciones.find(params[:id])
  end

  def valuacion_params
    params[:valuacion].permit(:numero,:ingeniero_inspector,:fecha_desde, :fecha_hasta, :monto_ejecucion, montos_valuaciones_attributes: [:valuacion_id, :desembolso_fuente_id, :moneda_id, :monto, :id, :_destroy])
  end  
end
