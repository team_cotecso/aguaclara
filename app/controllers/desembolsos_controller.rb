class DesembolsosController < ApplicationController
	before_filter :buscar_proyecto, :except => [:list]
	before_filter :buscar_proyectos_financiamientos
	before_filter :buscar_estatus_transferencia
	before_filter :buscar_fondos_financiamientos
	before_filter :buscar_desembolso, :except => [:index, :search, :new, :create, :list]

	def index
		@desembolsos = @proyecto.desembolsos.page params[:page]
	end
	def new
		@desembolso = Desembolso.new 
	end
	def create
		@desembolso = Desembolso.new(desembolso_params)
		@desembolso.proyecto = @proyecto
		if @desembolso.save
			flash[:notice] = "El Desembolso ha sido creada satisfactoriamente."
			redirect_to proyecto_desembolsos_path(@proyecto)
		else
			flash.now[:error] = "Ocurrieron errores al guardar."
      		render action: "new"
		end
	end
	def list
		@proyectos = Proyecto.order(:nombre)
		@desembolsos = Desembolso.order(:proyecto_id)
	end
	def search
		@desembolsos = Desembolso.order(:proyecto_id)
		@desembolsos = @desembolsos.where("estatus_id = ?", params[:desembolso][:estatus_transferencia_id]) unless params[:desembolso][:estatus_transferencia_id].blank?
		@desembolsos = @desembolsos.where("fecha >= ?", params[:desembolso][:desde]) unless params[:desembolso][:desde].blank?
		@desembolsos = @desembolsos.where("fecha <= ?", params[:desembolso][:hasta]) unless params[:desembolso][:hasta].blank?
		@desembolsos = @desembolsos.where("lower(proyectos.nombre) like ?",params[:desembolso][:proyecto]).joins(contrato: :proyecto) unless params[:desembolso][:proyecto].blank?
		@desembolsos = @desembolsos.where("lower(contratos.numero) like ?",params[:desembolso][:numero_contrato]).joins(:contrato) unless params[:desembolso][:numero_contrato].blank?

		unless params[:contrato_id].blank?
			render :index
		else
			render :list 
		end
	end
	def edit
		@desembolso = Desembolso.find(params[:id])
	end
	def destroy
		if @desembolso.destroy
			flash[:notice] = "El Desembolso ha sido eliminado satisfactoriamente."
			redirect_to proyecto_desembolsos_path(@proyecto)
		else
			flash.now[:error] = "Ocurrieron errores al eliminar."
      		render action: "index"
		end
	end
	def update
		if @desembolso.update(desembolso_params)
			flash[:notice] = "El Desembolso ha sido actualizado satisfactoriamente."
			redirect_to proyecto_desembolsos_path(@proyecto)
		else
			flash.now[:error] = "Ocurrieron errores al guardar."
      		render action: "edit"
		end
	end
	private
	def buscar_proyecto
		@proyecto = Proyecto.find(params[:proyecto_id]) if params[:proyecto_id]
	end
	def buscar_desembolso
		@desembolso = Desembolso.find(params[:id]) 
	end
	def buscar_fondos_financiamientos
		@fondos_financiamientos = FondoFinanciamiento.all
	end
	def buscar_estatus_transferencia
		@estatus_transferencia = EstatusTransferencia.all
	end
	def buscar_proyectos_financiamientos
		if @proyecto
			@proyectos_financiamientos = ProyectoFinanciamiento.where(:proyecto_id => @proyecto.id)
		else
			@proyectos_financiamientos = []
		end
	end
	def desembolso_params
    params.require(:desembolso).permit(:fecha, :monto, :valuacion, :observacion, :contrato_id, :fondo_financiamiento_id, :estatus_id, desembolsos_fuentes_attributes: [:desembolso_id, :proyecto_financiamiento_id, :moneda_id, :monto, :id, :_destroy])
  end
end
