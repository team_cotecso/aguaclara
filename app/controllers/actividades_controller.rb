class ActividadesController < ApplicationController
	before_filter :buscar_proyecto
	before_filter :buscar_tipos_actividades
	before_filter :buscar_actividad, :except => [:index, :search, :new, :create]

	def index
		@actividades = @proyecto.actividades.order(:fecha_inicial).page params[:page]
	end

	def new
		@actividad = Actividad.new 
		@actividad.proyecto = @proyecto
	end

	def create
		@actividad = Actividad.new(actividad_params)
		@actividad.proyecto = @proyecto
		if @actividad.save
			flash[:notice] = "La actividad ha sido creada satisfactoriamente."
			redirect_to proyecto_actividades_path(@proyecto)
		else
			flash.now[:error] = "Ocurrieron errores al guardar."
      render action: "new"
		end
	end

	def search
		@actividades = @proyecto.actividades.search(params[:actividad])
		render :index
	end

	def edit
		@actividad = Actividad.find(params[:id])
	end

	def destroy
		if @actividad.destroy
			flash[:notice] = "La actividad ha sido eliminada satisfactoriamente."
			redirect_to proyecto_actividades_path(@proyecto)
		else
			flash.now[:error] = "Ocurrieron errores al eliminar."
			render action: "index"
		end
	end
	def update
		if @actividad.update(actividad_params)
			flash[:notice] = "La actividad ha sido actualizada satisfactoriamente."
			redirect_to proyecto_actividades_path(@proyecto)
		else
			flash.now[:error] = "Ocurrieron errores al guardar."
      render action: "edit"
		end
	end
	private
	def buscar_proyecto
		@proyecto = Proyecto.find(params[:proyecto_id]) 
	end

	def buscar_actividad
		@actividad = Actividad.find(params[:id]) 
	end

	def buscar_tipos_actividades
		@tipos_actividades = TipoActividad.all
	end
	def actividad_params
    params.require(:actividad).permit(:descripcion, :tipo_actividad_id, :porcentaje_ejecucion, :peso, :fecha_inicial, :fecha_final, :observaciones)
  end
end
