class DependentsSelectsController < ApplicationController
  respond_to :json

  def entes_ejecutores
    institucion = Institucion.find params[:id]
    respond_with(institucion.entes_ejecutores)
  end

  def instituciones
    unidad = Unidad.find params[:id]
    respond_with(unidad.instituciones)
  end

  def fondos_financiamiento
    fuente = FuenteFinanciamiento.find params[:id]
    respond_with(fuente.fondos_financiamiento)
  end

  def sub_tipos_obras
    tipo = TipoObra.find params[:id]
    respond_with(tipo.sub_tipos_obras)
  end

  def municipios
    estado = EntidadTerritorial::Estado.find params[:id]
    respond_with(estado.municipios)
  end

  def parroquias
    municipio = EntidadTerritorial::Municipio.find params[:id]
    respond_with(municipio.parroquias)
  end

  def sectores
    parroquia = EntidadTerritorial::Parroquia.find params[:id]
    respond_with(parroquia.sectores)   
  end

end
