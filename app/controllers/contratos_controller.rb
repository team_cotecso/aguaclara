class ContratosController < ApplicationController
	before_filter :buscar_proyecto, :except => [:list]
	before_filter :buscar_contratistas
	before_filter :buscar_contrato, :only => [:edit, :update, :destroy]

	def index
		@contratos = @proyecto.contratos.page params[:page]
	end

	def list
		@proyectos = Proyecto.order(:nombre)
		@contratos = Contrato.order(:numero)
	end

	def new
		@contrato = Contrato.new 
		@contrato.proyecto = @proyecto
	end
	def create
		@contrato = Contrato.new(contrato_params)
		@contrato.proyecto = @proyecto
		if @contrato.save
			flash[:notice] = "El contrato ha sido creado satisfactoriamente."
			redirect_to proyecto_contratos_path(@proyecto)
		else
			puts @contrato.errors.to_json
			flash.now[:error] = "Ocurrieron errores al guardar."
      		render action: "new"
		end
	end
	def search
		@contratos = Contrato.search(params).order(:numero)
		unless params[:proyecto_id].blank?
			render :index
		else
			render :list 
		end
	end
	def edit
		@contrato = Contrato.find(params[:id])
	end
	def destroy
		if @contrato.destroy
			flash[:notice] = "El contrato ha sido eliminada satisfactoriamente."
			redirect_to proyecto_contratos_path(@proyecto)
		else
			flash.now[:error] = "Ocurrieron errores al eliminar."
      		render action: "index"
		end
	end
	def update
		if @contrato.update(contrato_params)
			flash[:notice] = "El contrato ha sido actualizada satisfactoriamente."
			redirect_to proyecto_contratos_path(@proyecto)
		else
			flash.now[:error] = "Ocurrieron errores al guardar."
      		render action: "edit"
		end
	end
	private
	def buscar_proyecto
		@proyecto = Proyecto.find(params[:proyecto_id]) if params[:proyecto_id] 
	end

	def buscar_contrato
		@contrato = Contrato.find(params[:id]) 
	end

	def buscar_contratistas
		@contratistas = Contratista.all
	end
	def contrato_params
    params.require(:contrato).permit(:numero, :ingeniero_residente, :proyecto_id, :contratista_id, :descripcion, :moneda_id, :monto)
  end
end
