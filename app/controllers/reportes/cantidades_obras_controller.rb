class Reportes::CantidadesObrasController < ApplicationController
	authorize_resource class: Reportes::CantidadesObrasController
	#Página principal del reporte. Alli es donde se visualizan los campos que filtran el reporte
	def index
	end

	def report
		@nombre_zona = "Cantidad de Proyectos por Estados"
		if params[:search][:sector_id].present? 
			@zonas = Sector.where(:id => params[:search][:sector_id])
			@nombre_zona = "Cantidad de Proyectos del Sector: #{@zonas.first.nombre}"
		elsif params[:search][:parroquia_id].present? 
			ubicacion = EntidadTerritorial::Parroquia.find(params[:search][:parroquia_id])
			@zonas = ubicacion.sectores
			@nombre_zona = "Cantidad de Proyectos por Sectores de la Parroquia: #{ubicacion.nombre}"
		elsif params[:search][:municipio_id].present? 
			ubicacion = EntidadTerritorial::Municipio.find(params[:search][:municipio_id])
			@zonas = ubicacion.parroquias			
			@nombre_zona = "Cantidad de Proyectos por Parroquias del Minicipio: #{ubicacion.nombre}"
		elsif params[:search][:estado_id].present? 
			ubicacion = EntidadTerritorial::Estado.find(params[:search][:estado_id])
			@zonas = ubicacion.municipios			
			@nombre_zona = "Cantidad de Proyectos por Municipios del Estado: #{ubicacion.nombre}"
		else
			@zonas = EntidadTerritorial::Estado.all
		end 
		
		render "report", :formats => [:pdf]
	end
end
