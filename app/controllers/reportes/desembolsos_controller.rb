class Reportes::DesembolsosController < ApplicationController
	authorize_resource class: Reportes::DesembolsosController
	#Página principal del reporte. Alli es donde se visualizan los campos que filtran el reporte
	def index
	end

	def report		
		@desembolsos = Desembolso.order(:proyecto_id)
		@desembolsos = @desembolsos.where("estatus_id = ?", params[:search][:estatus_transferencia_id]) unless params[:search][:estatus_transferencia_id].blank?
		@desembolsos = @desembolsos.where("fecha >= ?", params[:search][:desde]) unless params[:search][:desde].blank?
		@desembolsos = @desembolsos.where("fecha <= ?", params[:search][:hasta]) unless params[:search][:hasta].blank?
		@desembolsos = @desembolsos.where("proyecto_id = ?", params[:search][:proyecto_id]) unless params[:search][:proyecto_id].blank?

		render "report", :formats => [:pdf]
	end
end
