class Reportes::ActividadesController < ApplicationController
	authorize_resource class: Reportes::ActividadesController
	#Página principal del reporte. Alli es donde se visualizan los campos que filtran el reporte
	def index
	end

	def report
		@proyectos = Proyecto.search_by_query(params[:search])
		render "report", :formats => [:pdf]
	end
end
