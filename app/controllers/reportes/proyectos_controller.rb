class Reportes::ProyectosController < ApplicationController
	authorize_resource class: Reportes::ProyectosController
	#Página principal del reporte. Alli es donde se visualizan los campos que filtran el reporte
	def index
	end

	def report
		@proyectos = Proyecto.search_by_query(params[:search])
		render "report", :formats => [:pdf]
	end
end
