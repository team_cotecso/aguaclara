class BitacoraController < ApplicationController
  before_action :buscar_proyecto
  before_action :buscar_bitacora, only: [:edit, :update, :destroy]


  # GET /bitacoras
  def index
    @bitacoras = @proyecto.bitacoras
  end

  # GET /bitacoras/new
  def new
    @bitacora = @proyecto.bitacoras.new 
  end

  # POST /bitacoras
  def create
    @bitacora = @proyecto.bitacoras.new bitacora_params
    @bitacora.file = params[:bitacora][:file]
    @bitacora.usuario = current_user
    if @bitacora.save
      redirect_to proyecto_bitacoras_url, notice: 'La nota se ha creado satisfactoriamente.'
    else
      render action: 'new'
    end
  end

  # GET /bitacoras/:id/edit
  def edit
  end

  # PUT /bitacoras/:id
  # PATCH /bitacoras/:id
  def update
    @bitacora.file = params[:bitacora][:file]
    if @bitacora.update bitacora_params
      redirect_to proyecto_bitacoras_url, notice: 'La nota ha sido actualizado satisfactoriamente.'
    else
      render action: 'edit'
    end
  end

  # DELETE /bitacoras/:id
  def destroy
    if @bitacora.destroy
      redirect_to proyecto_bitacoras_url, notice: "La nota '#{@bitacora.titulo}' ha sido eliminada satisfactoriamente."
    else      
      redirect_to proyecto_bitacoras_url, notice: 'Ha ocurrido un error al eliminar la nota.<br/>'+@bitacora.errors.full_messages
    end      
  end 

private

  # Busca el proyecto especificado por +params[:proyecto_id]+
  # Llamado como before_action
  def buscar_proyecto
    @proyecto = Proyecto.find params[:proyecto_id]
  end  

  # Busca la bitácora especificada por +params[:id]+
  # Llamado como before_action
  def buscar_bitacora
    @bitacora = @proyecto.bitacoras.find(params[:id])
  end

  def bitacora_params
    params[:bitacora].permit(:titulo,:observaciones)
  end

end
