class ApplicationController < ActionController::Base
  before_action :authenticate_administrador!
  before_filter :configure_permitted_parameters, if: :devise_controller?

  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  # alias_method :current_user, :current_usuario
  # alias_method :current_admin, :current_administrador
  
  helper_method :current_user

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up) { |u| u.permit(:email, :password, :password_confirmation, :nombre, :apellido) }
    devise_parameter_sanitizer.for(:account_update) { |u| u.permit(:password, :password_confirmation, :current_password, :nombre) }        
  end
  
  def current_user
    @current_user ||= current_administrador
  end
  
  def after_sign_out_path_for(resource_or_scope)
    root_path
  end

end
