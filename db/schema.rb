# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20141112205926) do

  create_table "actividades", force: true do |t|
    t.string   "descripcion",          null: false
    t.integer  "peso",                 null: false
    t.integer  "porcentaje_ejecucion", null: false
    t.date     "fecha_inicial",        null: false
    t.date     "fecha_final",          null: false
    t.string   "observaciones"
    t.integer  "tipo_actividad_id",    null: false
    t.integer  "proyecto_id",          null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "actividades", ["proyecto_id"], name: "index_actividades_on_proyecto_id", using: :btree
  add_index "actividades", ["tipo_actividad_id"], name: "index_actividades_on_tipo_actividad_id", using: :btree

  create_table "administradores", force: true do |t|
    t.string   "email",                  default: "",       null: false
    t.string   "encrypted_password",     default: "",       null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,        null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "nombre",                 default: "nombre", null: false
    t.integer  "dependencia_id"
    t.integer  "roles_mask"
  end

  add_index "administradores", ["dependencia_id"], name: "index_administradores_on_dependencia_id", using: :btree
  add_index "administradores", ["email"], name: "index_administradores_on_email", unique: true, using: :btree
  add_index "administradores", ["reset_password_token"], name: "index_administradores_on_reset_password_token", unique: true, using: :btree

  create_table "bitacoras", force: true do |t|
    t.string   "titulo",            null: false
    t.text     "observaciones"
    t.integer  "proyecto_id",       null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "file_file_name"
    t.string   "file_content_type"
    t.integer  "file_file_size"
    t.datetime "file_updated_at"
    t.integer  "usuario_id",        null: false
  end

  add_index "bitacoras", ["proyecto_id"], name: "index_bitacoras_on_proyecto_id", using: :btree
  add_index "bitacoras", ["usuario_id"], name: "index_bitacoras_on_usuario_id", using: :btree

  create_table "contratistas", force: true do |t|
    t.string   "razon_social", null: false
    t.string   "rif",          null: false
    t.string   "direccion"
    t.string   "telefono"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "contratos", force: true do |t|
    t.string   "numero",                            null: false
    t.string   "ingeniero_residente",               null: false
    t.integer  "proyecto_id",                       null: false
    t.integer  "contratista_id",                    null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "descripcion",         default: "-", null: false
    t.integer  "moneda_id",                         null: false
    t.float    "monto",               default: 0.0, null: false
  end

  add_index "contratos", ["contratista_id"], name: "index_contratos_on_contratista_id", using: :btree
  add_index "contratos", ["proyecto_id"], name: "index_contratos_on_proyecto_id", using: :btree

  create_table "desembolsos", force: true do |t|
    t.date     "fecha"
    t.float    "monto"
    t.string   "valuacion"
    t.string   "observacion"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "estatus_id",  null: false
    t.integer  "proyecto_id", null: false
  end

  create_table "desembolsos_fuentes", force: true do |t|
    t.integer  "desembolso_id",              null: false
    t.float    "monto",                      null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "proyecto_financiamiento_id", null: false
    t.integer  "moneda_id",                  null: false
  end

  add_index "desembolsos_fuentes", ["desembolso_id"], name: "index_desembolsos_fuentes_on_desembolso_id", using: :btree

  create_table "entes_ejecutores", force: true do |t|
    t.string   "nombre",         null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "institucion_id", null: false
  end

  create_table "entidades_territoriales", force: true do |t|
    t.integer  "parent_id"
    t.string   "tipo",       null: false
    t.string   "nombre",     null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "entidades_territoriales", ["parent_id"], name: "index_entidades_territoriales_on_parent_id", using: :btree
  add_index "entidades_territoriales", ["tipo"], name: "index_entidades_territoriales_on_tipo", using: :btree

  create_table "estatus", force: true do |t|
    t.string   "nombre",     null: false
    t.string   "type",       null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "fondos_financiamiento", force: true do |t|
    t.string   "nombre",                   null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "fuente_financiamiento_id", null: false
  end

  create_table "fuentes_financiamiento", force: true do |t|
    t.string   "nombre",     null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "instituciones", force: true do |t|
    t.string   "nombre",     null: false
    t.integer  "unidad_id",  null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "instituciones", ["unidad_id"], name: "index_instituciones_on_unidad_id", using: :btree

  create_table "monedas", force: true do |t|
    t.string   "nombre",                       null: false
    t.boolean  "aumenta",      default: true,  null: false
    t.float    "valor",                        null: false
    t.boolean  "es_principal", default: false, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "simbolo",      default: "Bsf", null: false
  end

  create_table "montos_proyectos", force: true do |t|
    t.integer "proyecto_id", null: false
    t.integer "moneda_id",   null: false
    t.float   "aprobado",    null: false
    t.float   "solicitado",  null: false
  end

  add_index "montos_proyectos", ["proyecto_id"], name: "index_montos_proyectos_on_proyecto_id", using: :btree

  create_table "montos_valuaciones", force: true do |t|
    t.integer "valuacion_id",         null: false
    t.integer "moneda_id",            null: false
    t.integer "desembolso_fuente_id", null: false
    t.float   "monto",                null: false
  end

  add_index "montos_valuaciones", ["valuacion_id"], name: "index_montos_valuaciones_on_valuacion_id", using: :btree

  create_table "paises", force: true do |t|
    t.string   "codigo",       null: false
    t.string   "nombre",       null: false
    t.string   "nacionalidad", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "proyectos", force: true do |t|
    t.string   "nombre",                                                 null: false
    t.string   "descripcion",                                            null: false
    t.date     "fecha_inicial",                                          null: false
    t.date     "fecha_final",                                            null: false
    t.integer  "lapso_ejecucion_en_meses",                               null: false
    t.float    "monto_solicitado"
    t.float    "monto_aprobado"
    t.integer  "unidad_id",                                              null: false
    t.integer  "ente_ejecutor_id",                                       null: false
    t.integer  "institucion_id",                                         null: false
    t.integer  "estatus_id"
    t.integer  "tipo_obra_id",                                           null: false
    t.integer  "sub_tipo_obra_id",                                       null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "numero_proyecto",          limit: 15
    t.boolean  "impacto",                             default: false
    t.string   "punto_cuenta",             limit: 20, default: "-",      null: false
    t.boolean  "posee_recursos",                      default: false
    t.integer  "habitantes_beneficiados",             default: 0,        null: false
    t.integer  "empleos_directos",                    default: 0,        null: false
    t.integer  "empleos_indirectos",                  default: 0,        null: false
    t.string   "estatus_dinamico",                    default: "inicio", null: false
  end

  add_index "proyectos", ["ente_ejecutor_id"], name: "index_proyectos_on_ente_ejecutor_id", using: :btree
  add_index "proyectos", ["estatus_id"], name: "index_proyectos_on_estatus_id", using: :btree
  add_index "proyectos", ["institucion_id"], name: "index_proyectos_on_institucion_id", using: :btree
  add_index "proyectos", ["sub_tipo_obra_id"], name: "index_proyectos_on_sub_tipo_obra_id", using: :btree
  add_index "proyectos", ["tipo_obra_id"], name: "index_proyectos_on_tipo_obra_id", using: :btree
  add_index "proyectos", ["unidad_id"], name: "index_proyectos_on_unidad_id", using: :btree

  create_table "proyectos_financiamientos", force: true do |t|
    t.integer  "proyecto_id",              null: false
    t.integer  "fuente_financiamiento_id", null: false
    t.integer  "fondo_financiamiento_id",  null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "proyectos_financiamientos", ["fondo_financiamiento_id"], name: "index_proyectos_financiamientos_on_fondo_financiamiento_id", using: :btree
  add_index "proyectos_financiamientos", ["fuente_financiamiento_id"], name: "index_proyectos_financiamientos_on_fuente_financiamiento_id", using: :btree
  add_index "proyectos_financiamientos", ["proyecto_id"], name: "index_proyectos_financiamientos_on_proyecto_id", using: :btree

  create_table "proyectos_ubicaciones", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "proyecto_id",  null: false
    t.integer  "sector_id",    null: false
    t.integer  "estado_id",    null: false
    t.integer  "municipio_id", null: false
    t.integer  "parroquia_id", null: false
  end

  create_table "rich_rich_files", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "rich_file_file_name"
    t.string   "rich_file_content_type"
    t.integer  "rich_file_file_size"
    t.datetime "rich_file_updated_at"
    t.string   "owner_type"
    t.integer  "owner_id"
    t.text     "uri_cache"
    t.string   "simplified_type",        default: "file"
  end

  create_table "sectores", force: true do |t|
    t.string   "nombre"
    t.integer  "poblacion"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "entidad_territorial_id", null: false
  end

  create_table "sub_tipos_obras", force: true do |t|
    t.string   "nombre",       null: false
    t.integer  "tipo_obra_id", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "sub_tipos_obras", ["tipo_obra_id"], name: "index_sub_tipos_obras_on_tipo_obra_id", using: :btree

  create_table "tipos_actividades", force: true do |t|
    t.string   "nombre",     null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "tipos_obras", force: true do |t|
    t.string   "nombre",     null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "unidades", force: true do |t|
    t.string   "nombre",     null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "usuarios", force: true do |t|
    t.string   "email",                             default: "",         null: false
    t.string   "encrypted_password",                default: "",         null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                     default: 0,          null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "nombre",                 limit: 30, default: "Nombre",   null: false
    t.string   "apellido",               limit: 30, default: "Apellido", null: false
  end

  add_index "usuarios", ["email"], name: "index_usuarios_on_email", unique: true, using: :btree
  add_index "usuarios", ["reset_password_token"], name: "index_usuarios_on_reset_password_token", unique: true, using: :btree

  create_table "valuaciones", force: true do |t|
    t.string   "numero",                          null: false
    t.integer  "contrato_id",                     null: false
    t.string   "ingeniero_inspector", limit: 100, null: false
    t.text     "observaciones"
    t.date     "fecha_desde",                     null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.date     "fecha_hasta",                     null: false
  end

  add_index "valuaciones", ["contrato_id"], name: "index_valuaciones_on_contrato_id", using: :btree
  add_index "valuaciones", ["numero"], name: "index_valuaciones_on_numero", unique: true, using: :btree

  create_table "versions", force: true do |t|
    t.string   "item_type",      null: false
    t.integer  "item_id",        null: false
    t.string   "event",          null: false
    t.string   "whodunnit"
    t.text     "object"
    t.datetime "created_at"
    t.text     "object_changes"
  end

  add_index "versions", ["item_type", "item_id"], name: "index_versions_on_item_type_and_item_id", using: :btree

  add_foreign_key "actividades", "proyectos", name: "actividades_proyecto_id_fk"
  add_foreign_key "actividades", "tipos_actividades", name: "actividades_tipo_actividad_id_fk"

  add_foreign_key "bitacoras", "administradores", name: "bitacoras_usuario_id_fk", column: "usuario_id"
  add_foreign_key "bitacoras", "proyectos", name: "bitacoras_proyecto_id_fk"

  add_foreign_key "contratos", "contratistas", name: "contratos_contratista_id_fk"
  add_foreign_key "contratos", "monedas", name: "contratos_moneda_id_fk"
  add_foreign_key "contratos", "proyectos", name: "contratos_proyecto_id_fk"

  add_foreign_key "desembolsos", "estatus", name: "desembolsos_estatus_id_fk"
  add_foreign_key "desembolsos", "proyectos", name: "desembolsos_proyecto_id_fk"

  add_foreign_key "desembolsos_fuentes", "desembolsos", name: "desembolsos_fuentes_desembolso_id_fk"
  add_foreign_key "desembolsos_fuentes", "monedas", name: "desembolsos_fuentes_moneda_id_fk"
  add_foreign_key "desembolsos_fuentes", "proyectos_financiamientos", name: "desembolsos_fuentes_proyecto_financiamiento_id_fk"

  add_foreign_key "entes_ejecutores", "instituciones", name: "entes_ejecutores_institucion_id_fk"

  add_foreign_key "entidades_territoriales", "entidades_territoriales", name: "entidades_territoriales_parent_id_fk", column: "parent_id"

  add_foreign_key "fondos_financiamiento", "fuentes_financiamiento", name: "fondos_financiamiento_fuente_financiamiento_id_fk"

  add_foreign_key "instituciones", "unidades", name: "instituciones_unidad_id_fk"

  add_foreign_key "montos_proyectos", "monedas", name: "montos_proyectos_moneda_id_fk"
  add_foreign_key "montos_proyectos", "proyectos", name: "montos_proyectos_proyecto_id_fk"

  add_foreign_key "montos_valuaciones", "desembolsos_fuentes", name: "montos_valuaciones_desembolso_fuente_id_fk"
  add_foreign_key "montos_valuaciones", "monedas", name: "montos_valuaciones_moneda_id_fk"
  add_foreign_key "montos_valuaciones", "valuaciones", name: "montos_valuaciones_valuacion_id_fk"

  add_foreign_key "proyectos", "entes_ejecutores", name: "proyectos_ente_ejecutor_id_fk"
  add_foreign_key "proyectos", "estatus", name: "proyectos_estatus_id_fk"
  add_foreign_key "proyectos", "instituciones", name: "proyectos_institucion_id_fk"
  add_foreign_key "proyectos", "sub_tipos_obras", name: "proyectos_sub_tipo_obra_id_fk"
  add_foreign_key "proyectos", "tipos_obras", name: "proyectos_tipo_obra_id_fk"
  add_foreign_key "proyectos", "unidades", name: "proyectos_unidad_id_fk"

  add_foreign_key "proyectos_financiamientos", "fondos_financiamiento", name: "proyectos_financiamientos_fondo_financiamiento_id_fk"
  add_foreign_key "proyectos_financiamientos", "fuentes_financiamiento", name: "proyectos_financiamientos_fuente_financiamiento_id_fk"
  add_foreign_key "proyectos_financiamientos", "proyectos", name: "proyectos_financiamientos_proyecto_id_fk"

  add_foreign_key "proyectos_ubicaciones", "entidades_territoriales", name: "proyectos_ubicaciones_estado_id_fk", column: "estado_id"
  add_foreign_key "proyectos_ubicaciones", "entidades_territoriales", name: "proyectos_ubicaciones_municipio_id_fk", column: "municipio_id"
  add_foreign_key "proyectos_ubicaciones", "entidades_territoriales", name: "proyectos_ubicaciones_parroquia_id_fk", column: "parroquia_id"
  add_foreign_key "proyectos_ubicaciones", "proyectos", name: "proyectos_ubicaciones_proyecto_id_fk"
  add_foreign_key "proyectos_ubicaciones", "sectores", name: "proyectos_ubicaciones_sector_id_fk"

  add_foreign_key "sectores", "entidades_territoriales", name: "sectores_entidad_territorial_id_fk"

  add_foreign_key "sub_tipos_obras", "tipos_obras", name: "sub_tipos_obras_tipo_obra_id_fk"

  add_foreign_key "valuaciones", "contratos", name: "valuaciones_contrato_id_fk"

end
