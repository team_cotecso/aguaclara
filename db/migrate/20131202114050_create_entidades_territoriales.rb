class CreateEntidadesTerritoriales < ActiveRecord::Migration
  def change
    create_table :entidades_territoriales do |t|
      t.integer :parent_id
      t.string :tipo, null: false
      t.string :nombre, null: false
      
      t.timestamps
    end
    
    add_foreign_key(:entidades_territoriales, :entidades_territoriales, column: 'parent_id')
    add_index :entidades_territoriales, :parent_id
    add_index :entidades_territoriales, :tipo
  end
end
