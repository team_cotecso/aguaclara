class AgregarEstatusTransferenciaIdADesembolso < ActiveRecord::Migration
  def change
  	add_column :desembolsos, :estatus_id, :integer, :null => false

  	add_foreign_key(:desembolsos, :estatus)
  end
end
