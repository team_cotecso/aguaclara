class CreateTiposObras < ActiveRecord::Migration
  def change
    create_table :tipos_obras do |t|
      t.string :nombre, null: false

      t.timestamps
    end
  end
end
