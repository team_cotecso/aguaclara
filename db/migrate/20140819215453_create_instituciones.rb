class CreateInstituciones < ActiveRecord::Migration
  def change
    create_table :instituciones do |t|
      t.string :nombre, null: false
      t.references :unidad, index: true, null: false

      t.timestamps
    end
    
    add_foreign_key(:instituciones, :unidades)
  end
end
