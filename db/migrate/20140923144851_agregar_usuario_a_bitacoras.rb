class AgregarUsuarioABitacoras < ActiveRecord::Migration
  def change
    Bitacora.destroy_all
    add_column :bitacoras, :usuario_id, :integer, null: false
    add_foreign_key(:bitacoras, :administradores, column: 'usuario_id')
    add_index :bitacoras, :usuario_id    
  end
end
