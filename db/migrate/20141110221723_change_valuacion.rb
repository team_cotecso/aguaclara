class ChangeValuacion < ActiveRecord::Migration
  def change
    rename_column :valuaciones, :fecha, :fecha_desde
    add_column :valuaciones, :fecha_hasta, :date, null: false
  end
end
