class CreateDesembolsosFuentes < ActiveRecord::Migration
  def change
    create_table :desembolsos_fuentes do |t|
      t.references :desembolso, index: true, null: false
      t.references :fuente_financiamiento, index: true, null: false
      t.float :monto, null: false

      t.timestamps
    end
    
    add_foreign_key(:desembolsos_fuentes, :desembolsos)
    add_foreign_key(:desembolsos_fuentes, :fuentes_financiamiento)
  end
end
