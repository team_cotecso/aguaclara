class AgregarPuntoDePuentaAProyecto < ActiveRecord::Migration
  def change
  	add_column :proyectos, :punto_cuenta, :string, limit: 20,  null: false, default: "-"
  end
end
