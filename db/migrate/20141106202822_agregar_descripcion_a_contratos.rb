class AgregarDescripcionAContratos < ActiveRecord::Migration
  def change
  	add_column :contratos, :descripcion, :string, limit: 255,  null: false, default: "-"
  end
end
