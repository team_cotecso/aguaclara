class CreateProyectos < ActiveRecord::Migration
  def change
    create_table :proyectos do |t|
      t.string :nombre, null: false
      t.string :descripcion, null: false
      t.date :fecha_inicial, null: false
      t.date :fecha_final, null: false
      t.integer :lapso_ejecucion_en_meses, null: false
      t.float :monto_solicitado, null: false
      t.float :monto_aprobado, null: false
      t.references :unidad, index: true, null: false
      t.references :ente_ejecutor, index: true, null: false
      t.references :institucion, index: true, null: false
      t.references :fondo_financiamiento, index: true, null: false
      t.references :fuente_financiamiento, index: true, null: false
      t.references :estatus, index: true
      t.references :impacto, index: true, null: false
      t.references :tipo_obra, index: true, null: false
      t.references :sub_tipo_obra, index: true, null: false

      t.timestamps
    end

    add_foreign_key(:proyectos, :unidades)
    add_foreign_key(:proyectos, :entes_ejecutores)
    add_foreign_key(:proyectos, :instituciones)
    add_foreign_key(:proyectos, :fondos_financiamiento)
    add_foreign_key(:proyectos, :fuentes_financiamiento)
    add_foreign_key(:proyectos, :estatus)
    add_foreign_key(:proyectos, :impactos)
    add_foreign_key(:proyectos, :tipos_obras)
    add_foreign_key(:proyectos, :sub_tipos_obras)
  end
end
