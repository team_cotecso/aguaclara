class AddEstadoToProyecto < ActiveRecord::Migration
  def change
    add_column :proyectos, :estatus_dinamico, :string, null: false, default: "inicio"
  end
end
