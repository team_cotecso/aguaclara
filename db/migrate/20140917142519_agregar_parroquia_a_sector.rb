class AgregarParroquiaASector < ActiveRecord::Migration
  def change
  	add_column :sectores, :entidad_territorial_id, :integer, :null => false

  	add_foreign_key(:sectores, :entidades_territoriales)
  end
end
