class CreateMoneda < ActiveRecord::Migration
  def change
    create_table :monedas do |t|
      t.string :nombre, index: true, null: false
      t.boolean :aumenta, null: false, default: true
      t.float :valor, null: false
      t.boolean :es_principal, null: false, default: false
      t.timestamps      
    end 
  end
end
