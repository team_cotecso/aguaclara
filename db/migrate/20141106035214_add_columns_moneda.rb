class AddColumnsMoneda < ActiveRecord::Migration
  def change
    add_column :monedas, :simbolo, :string, null: false, default: "Bsf"
    add_column :montos_proyectos, :aprobado, :float, null: false
    add_column :montos_proyectos, :solicitado, :float, null: false

    remove_column :montos_proyectos, :monto
  end
end
