class CreateDesembolsos < ActiveRecord::Migration
  def change
    create_table :desembolsos do |t|
      t.date :fecha
      t.float :monto
      t.string :valuacion
      t.string :observacion
      t.references :contrato, index: true

      t.timestamps
    end

    add_foreign_key(:desembolsos, :contratos)
  end
end
