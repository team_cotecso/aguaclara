class ChangeContrato < ActiveRecord::Migration
  def change
    Valuacion.destroy_all
    Contrato.destroy_all
    add_column :contratos, :moneda_id, :integer, null: false
    add_foreign_key(:contratos, :monedas)

    add_column :contratos, :monto, :float, null: false, default: 0
  end
end
