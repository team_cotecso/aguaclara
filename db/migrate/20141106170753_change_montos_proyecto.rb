class ChangeMontosProyecto < ActiveRecord::Migration
  def change
    change_column_null :proyectos, :monto_solicitado, true
    change_column_null :proyectos, :monto_aprobado, true
  end
end
