class CreateSectores < ActiveRecord::Migration
  def change
    create_table :sectores do |t|
      t.string :nombre
      t.integer :poblacion

      t.timestamps
    end
  end
end
