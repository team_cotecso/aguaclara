class CreateImpactos < ActiveRecord::Migration
  def change
    create_table :impactos do |t|
      t.string :nombre, null: false

      t.timestamps
    end
  end
end
