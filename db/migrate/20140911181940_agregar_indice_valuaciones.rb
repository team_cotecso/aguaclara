class AgregarIndiceValuaciones < ActiveRecord::Migration
  def change
    add_index(:valuaciones, :numero, unique: true)
  end
end
