class CreateFuentesFinanciamiento < ActiveRecord::Migration
  def change
    create_table :fuentes_financiamiento do |t|
      t.string :nombre, null: false
      t.references :fondo_financiamiento, index: true, null: false
      
      t.timestamps
    end
    
    add_foreign_key(:fuentes_financiamiento, :fondos_financiamiento)
  end
end
