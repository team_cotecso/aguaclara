class AgregarForeingKeysEnTablaProyectosSectores < ActiveRecord::Migration
  def change
  	add_column :proyectos_sectores, :proyecto_id, :integer, :null => false
  	add_column :proyectos_sectores, :sector_id, :integer, :null => false

  	add_foreign_key(:proyectos_sectores, :proyectos)
  	add_foreign_key(:proyectos_sectores, :sectores)
  end


end
