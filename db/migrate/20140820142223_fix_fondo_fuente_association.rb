class FixFondoFuenteAssociation < ActiveRecord::Migration
  def change
    add_column :fondos_financiamiento, :fuente_financiamiento_id, :integer, index: true, null: false
    remove_column :fuentes_financiamiento, :fondo_financiamiento_id
    
    add_foreign_key(:fondos_financiamiento, :fuentes_financiamiento)
  end
end