class CreateMontoProyecto < ActiveRecord::Migration
  def change
    create_table :montos_proyectos do |t|
      t.references :proyecto, index: true, null: false
      t.references :moneda, null: false
      t.float :monto, null: false
    end
    add_foreign_key(:montos_proyectos, :proyectos)
    add_foreign_key(:montos_proyectos, :monedas)
  end
end
