class CreateSubTiposObras < ActiveRecord::Migration
  def change
    create_table :sub_tipos_obras do |t|
      t.string :nombre, null: false
      t.references :tipo_obra, index: true, null: false

      t.timestamps
    end
    
    add_foreign_key(:sub_tipos_obras, :tipos_obras)
  end
end
