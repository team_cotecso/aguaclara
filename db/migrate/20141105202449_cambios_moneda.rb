class CambiosMoneda < ActiveRecord::Migration
  def change
    # DesembolsoFuente
    remove_foreign_key(:desembolsos_fuentes, :fuentes_financiamiento)
    remove_column :desembolsos_fuentes, :fuente_financiamiento_id

    add_column :desembolsos_fuentes, :proyecto_financiamiento_id, :integer, index: true
    DesembolsoFuente.update_all(:proyecto_financiamiento_id => ProyectoFinanciamiento.first) if DesembolsoFuente.any?
    change_column :desembolsos_fuentes, :proyecto_financiamiento_id, :integer, null: false
    add_foreign_key(:desembolsos_fuentes, :proyectos_financiamientos)
    add_column :desembolsos_fuentes, :moneda_id, :integer, index: true
    DesembolsoFuente.update_all(:moneda_id => Moneda.first) if DesembolsoFuente.any?
    change_column :desembolsos_fuentes, :moneda_id, :integer, null: false
    add_foreign_key(:desembolsos_fuentes, :monedas)

  end
end
