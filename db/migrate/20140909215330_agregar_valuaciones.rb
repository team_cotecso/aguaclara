class AgregarValuaciones < ActiveRecord::Migration
  def change
    create_table :valuaciones do |t|
      t.string :numero, null: false
      t.float :monto_ejecucion, null:false
      t.references :contrato, index: true, null: false
      t.string :ingeniero_inspector, null: false, limit: 100
      t.text :observaciones
      t.date :fecha, null:false
      t.timestamps
    end    

    add_foreign_key(:valuaciones, :contratos)
    
  end

 
end
