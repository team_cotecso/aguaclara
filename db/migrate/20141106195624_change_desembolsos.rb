class ChangeDesembolsos < ActiveRecord::Migration
  def change
    remove_column :desembolsos, :fondo_financiamiento_id, :integer
    remove_column :desembolsos, :contrato_id, :integer

    add_column :desembolsos, :proyecto_id, :integer
    Desembolso.update_all(proyecto_id: Proyecto.first) if Desembolso.any?
    change_column :desembolsos, :proyecto_id, :integer, null: false
    add_foreign_key(:desembolsos, :proyectos)

    change_column_null :desembolsos, :monto, true
  end
end
