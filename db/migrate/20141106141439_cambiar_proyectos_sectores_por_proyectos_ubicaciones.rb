class CambiarProyectosSectoresPorProyectosUbicaciones < ActiveRecord::Migration
  def change
    
    remove_foreign_key(:proyectos_sectores, {:column=>"proyecto_id"})
    remove_foreign_key(:proyectos_sectores, {:column=>"sector_id"})
    

    rename_table :proyectos_sectores, :proyectos_ubicaciones
    
    add_column :proyectos_ubicaciones, :estado_id, :integer, null: false
    add_foreign_key(:proyectos_ubicaciones, :entidades_territoriales, column: 'estado_id')
    add_column :proyectos_ubicaciones, :municipio_id, :integer
    add_foreign_key(:proyectos_ubicaciones, :entidades_territoriales, column: 'municipio_id')
    add_column :proyectos_ubicaciones, :parroquia_id, :integer
    add_foreign_key(:proyectos_ubicaciones, :entidades_territoriales, column: 'parroquia_id')
    remove_column :proyectos_ubicaciones, :sector_id
    add_column :proyectos_ubicaciones, :sector_id, :integer
    add_foreign_key(:proyectos_ubicaciones, :sectores, column: 'sector_id')

    add_foreign_key(:proyectos_ubicaciones, :proyectos, column: 'proyecto_id')
    
  end
end
