class CreateEstatus < ActiveRecord::Migration
  def change
    create_table :estatus do |t|
      t.string :nombre, null: false
      t.string :type, index: true, null: false

      t.timestamps
    end
  end
end
