class AgregarBitacoras < ActiveRecord::Migration
  def self.up
    
    create_table :bitacoras do |t|
      t.string :titulo, null: false
      t.text :observaciones
      t.references :proyecto, index: true, null: false
      t.timestamps
    end    

    add_attachment :bitacoras, :file
    add_foreign_key(:bitacoras, :proyectos)    
   
  end

  def self.down
    remove_foreign_key(:bitacoras, name: 'bitacoras_proyectos_proyecto_id_foreign_key')
    remove_attachment :bitacoras, :file
    drop_table :bitacoras
  end  
end
