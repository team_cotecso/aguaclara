class CreateFondosFinanciamiento < ActiveRecord::Migration
  def change
    create_table :fondos_financiamiento do |t|
      t.string :nombre, null: false

      t.timestamps
    end
  end
end
