class CreateMontosValuaciones < ActiveRecord::Migration
  def change
    create_table :montos_valuaciones do |t|
      t.references :valuacion, index: true, null: false
      t.references :moneda, null: false
      t.references :desembolso_fuente, null: false
      t.float :monto, null: false
    end
    add_foreign_key(:montos_valuaciones, :valuaciones)
    add_foreign_key(:montos_valuaciones, :monedas)
    add_foreign_key(:montos_valuaciones, :desembolsos_fuentes)

    remove_column :valuaciones, :monto_ejecucion, :float
  end
end
