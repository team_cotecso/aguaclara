class CreatePaises < ActiveRecord::Migration
  def change
    create_table :paises do |t|
      t.string :codigo, null: false
      t.string :nombre, null: false
      t.string :nacionalidad, null: false

      t.timestamps
    end
  end
end
