class CreateUnidades < ActiveRecord::Migration
  def change
    create_table :unidades do |t|
      t.string :nombre, null: false

      t.timestamps
    end
  end
end
