class CambiarUnidadPorInstitucionEnEnteEjecutor < ActiveRecord::Migration
  def change
  	remove_column :entes_ejecutores, :unidad_id
  	add_column :entes_ejecutores, :institucion_id, :integer, index: true
    EnteEjecutor.update_all(:institucion_id => Institucion.first) if EnteEjecutor.any?
    change_column :entes_ejecutores, :institucion_id, :integer, null: false
    add_foreign_key(:entes_ejecutores, :instituciones)
  end
end
