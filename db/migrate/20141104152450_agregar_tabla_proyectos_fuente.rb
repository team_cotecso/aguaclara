class AgregarTablaProyectosFuente < ActiveRecord::Migration
  def change
    create_table :proyectos_financiamientos do |t|
      t.references :proyecto, index: true, null: false
      t.references :fuente_financiamiento, index: true, null: false
      t.references :fondo_financiamiento, index: true, null: false
      t.timestamps
    end
    add_foreign_key(:proyectos_financiamientos, :proyectos)
    add_foreign_key(:proyectos_financiamientos, :fuentes_financiamiento)
    add_foreign_key(:proyectos_financiamientos, :fondos_financiamiento)
  end

end
