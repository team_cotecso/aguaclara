class CambiarImpactoABoolEnProyectosEliminarTablaImpacto < ActiveRecord::Migration
  def change
    remove_column :proyectos, :impacto_id
    add_column :proyectos, :impacto, :bool, default: false
    drop_table :impactos
  end
end
