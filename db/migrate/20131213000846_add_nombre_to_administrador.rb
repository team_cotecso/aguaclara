class AddNombreToAdministrador < ActiveRecord::Migration
  def change
    add_column :administradores, :nombre, :string, :null => false, default: "nombre"
    add_reference :administradores, :dependencia, index: true
  end
end
