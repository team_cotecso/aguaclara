class CreateActividades < ActiveRecord::Migration
  def change
    create_table :actividades do |t|
      t.string :descripcion, null: false
      t.integer :peso, null: false
      t.integer :porcentaje_ejecucion, null: false
      t.date :fecha_inicial, null: false
      t.date :fecha_final, null: false
      t.string :observaciones
      t.references :tipo_actividad, index: true, null: false
      t.references :proyecto, index: true, null: false

      t.timestamps
    end

    add_foreign_key(:actividades, :tipos_actividades)
    add_foreign_key(:actividades, :proyectos)
  end
end
