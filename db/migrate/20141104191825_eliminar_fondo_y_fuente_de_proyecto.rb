class EliminarFondoYFuenteDeProyecto < ActiveRecord::Migration
  def change
  	remove_column :proyectos, :fondo_financiamiento_id
  	remove_column :proyectos, :fuente_financiamiento_id
  end
end
