class AgregarCamposAProyectos < ActiveRecord::Migration
  def change
  	add_column :proyectos, :posee_recursos, :bool, default: false
  	add_column :proyectos, :habitantes_beneficiados, :integer, null: false, default: 0
  	add_column :proyectos, :empleos_directos, :integer, null: false, default: 0
  	add_column :proyectos, :empleos_indirectos, :integer, null: false, default: 0
  end
end
