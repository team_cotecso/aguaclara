class CreateContratos < ActiveRecord::Migration
  def change
    create_table :contratos do |t|
      t.string :numero, null: false
      t.string :ingeniero_residente, null: false
      t.references :proyecto, index: true, null: false
      t.references :contratista, index: true, null: false

      t.timestamps
    end

    add_foreign_key(:contratos, :proyectos)
    add_foreign_key(:contratos, :contratistas)
  end
end
