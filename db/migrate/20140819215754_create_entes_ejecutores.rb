class CreateEntesEjecutores < ActiveRecord::Migration
  def change
    create_table :entes_ejecutores do |t|
      t.string :nombre, null: false
      t.references :unidad, index: true, null: false

      t.timestamps
    end
    
    add_foreign_key(:entes_ejecutores, :unidades)
  end
end
