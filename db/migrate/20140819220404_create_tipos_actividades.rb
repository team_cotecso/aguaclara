class CreateTiposActividades < ActiveRecord::Migration
  def change
    create_table :tipos_actividades do |t|
      t.string :nombre, :null => false

      t.timestamps
    end
  end
end
