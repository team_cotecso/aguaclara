class CreateContratistas < ActiveRecord::Migration
  def change
    create_table :contratistas do |t|
      t.string :razon_social, null: false
      t.string :rif, null: false
      t.string :direccion
      t.string :telefono

      t.timestamps
    end
  end
end
