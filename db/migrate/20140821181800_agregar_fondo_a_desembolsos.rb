class AgregarFondoADesembolsos < ActiveRecord::Migration
  def change
    add_column :desembolsos, :fondo_financiamiento_id, :integer, index: true, null: false
    add_foreign_key(:desembolsos, :fondos_financiamiento)
  end
end