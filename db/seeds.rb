# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


# Carga de Estado
puts "Cargando  Estado desde csv/estado.csv..."
EntidadTerritorial::Parroquia.destroy_all
EntidadTerritorial::Municipio.destroy_all
EntidadTerritorial::Ciudad.destroy_all
EntidadTerritorial::Estado.destroy_all
File.readlines(File.join(Rails.root, "db", "csv", "estado.csv")).each do |l|
	data = l.chomp.split(';')
	r = EntidadTerritorial::Estado.create(id: data[0],nombre: data[1])
	puts r.errors.full_messages if r.new_record?
	puts r.inspect if r.new_record?
end

# Carga de Municipio
puts "Cargando  Municipio desde csv/municipio.csv..."
File.readlines(File.join(Rails.root, "db", "csv", "municipio.csv")).each do |l|
	data = l.chomp.split(';')
	r = EntidadTerritorial::Municipio.create(id: data[0], parent_id: data[1], nombre: data[2])
	puts r.errors.full_messages if r.new_record?
	puts r.inspect if r.new_record?
end

# Carga de ciudad
puts "Cargando  ciudad desde csv/ciudad.csv..."
item = 781
File.readlines(File.join(Rails.root, "db", "csv", "ciudad.csv")).each do |l|
	item=item+1
	data = l.chomp.split(';')
	r = EntidadTerritorial::Ciudad.create(id: item, parent_id: data[0], nombre: data[1])
	puts r.errors.full_messages if r.new_record?
	puts r.inspect if r.new_record?
end

# Carga de parroquia
puts "Cargando  parroquia desde csv/parroquia.csv..."
File.readlines(File.join(Rails.root, "db", "csv", "parroquia.csv")).each do |l|
	item=item+1
	data = l.chomp.split(';')
	r = EntidadTerritorial::Parroquia.create(id: item, parent_id: data[0], nombre: data[1])
	puts r.errors.full_messages if r.new_record?
	puts r.inspect if r.new_record?
end


# Carga Paises
puts "Cargando Paises desde csv/paises.csv..."
Pais.destroy_all
File.readlines(File.join(Rails.root, "db", "csv", "paises.csv")).each do |l|
	data = l.chomp.split(';')
	r = Pais.create(codigo: data[0], nombre: data[1], nacionalidad: data[6] )
	puts r.errors.full_messages if r.new_record?
	puts r.inspect if r.new_record?
end

Administrador.create(:email => "admin@minamb.gob.ve", :password => "12345678", :password_confirmation => "12345678", :roles_mask => 1)
puts "Usuario Administrador Creado:"
puts "Correo: admin@minamb.gob.ve"
puts "password: 12345678"

